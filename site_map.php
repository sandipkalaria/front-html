<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap sitemap-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul>
                    <li><a title="Home" href="#"><span class="sp-icon"><i class="fa fa-home"></i></span><span class="sp-main-menu">Home</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a>
                                <ul>
                                    <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                </ul>
                            </li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-newspaper-o"></i></span><span class="sp-main-menu">Main Menu Item</span></a></li>
                    <li><a title="Title Here" href="#"><span class="sp-icon"><i class="fa fa-sitemap"></i></span><span class="sp-main-menu">XML Sitemap</span></a></li>
                </ul>
            </div>
            <div class="col-sm-12">
                <div class="ac-title n-mt-sm-50 n-mt-25 n-mb-25">Socia Media</div>
                <ul>
                    <li>
                        <a target="_blank" title="Facebook" href="#">
                            <span class="sp-icon"><i class="fa fa-facebook"></i></span>
                            <span class="sp-main-menu">Facebook</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" title="Twitter" href="#">
                            <span class="sp-icon"><i class="fa fa-twitter"></i></span>
                            <span class="sp-main-menu">Twitter</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" title="YouTube" href="#">
                            <span class="sp-icon"><i class="fa fa-youtube"></i></span>
                            <span class="sp-main-menu">YouTube</span>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" title="Instagram" href="#">
                            <span class="sp-icon"><i class="fa fa-instagram"></i></span>
                            <span class="sp-main-menu">Instagram</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>