<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap about-us-03">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 col-lg-5">
                <h2 class="ac-title">The standard Lorem Ipsum passage</h2>
                <div class="cms n-mt-30">
                    <p>Our values mold the way we work with our clients, delivering excellent user experience supported by cutting-edge technologies. Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years. Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years.</p>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 offset-xl-1">
                <div class="thumbnail-container object-fit n-bs-5 n-mt-20 n-mt-lg-0" data-thumb="66.66%">
                    <div class="thumbnail">
                        <picture>
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/about-us.webp" srcset="assets/images/loader.svg">
                            <img class="lazy" data-src="assets/images/others/about-us.jpg" src="assets/images/loader.svg" alt="" title="" width="900" height="537" />
                        </picture>
                    </div>
                </div>
                <div class="-img-title p-tag"><span class="ac-iptitle n-mb-10 d-block">What is Lorem Ipsum?</span> There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...</div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>