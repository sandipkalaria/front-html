<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h2 class="ac-iptitle n-mb-15">Button Tag Button</h2>
                <button type="button" class="ac-btn-primary" title="Primary">Primary</button>
                <button type="button" class="ac-btn-secondary" title="Secondary">Secondary</button>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h2 class="ac-iptitle n-mb-15">A Tag Button</h2>
                <a href="javascript:void(0);" class="ac-btn-primary" title="Primary">Primary</a>
                <a href="javascript:void(0);" class="ac-btn-secondary" title="Secondary">Secondary</a>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Outline Button Tag</h3>
                <button type="button" class="ac-btn-outline-primary" title="Primary">Primary</button>
                <button type="button" class="ac-btn-outline-secondary" title="Secondary">Secondary</button>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">A Tag Outline Button</h3>
                <a href="javascript:void(0);" class="ac-btn-outline-primary" title="Primary">Primary</a>
                <a href="javascript:void(0);" class="ac-btn-outline-secondary" title="Secondary">Secondary</a>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Breadcrumb</h3>
                <ul class="ac-breadcrumb d-block n-mt-15 n-fs-14 fw-normal n-fc-black">
                    <li><a class="n-ah-b" href="#" title="Home">Home</a></li>
                    <li><a href="#" title="Library">Library</a></li>
                    <li class="active">Data</li>
                </ul>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Modal</h3>
                <button type="button" class="ac-btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Launch modal</button>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Pagination</h3>
                <ul class="ac-pagination">
                    <li><a href="javascript:void(0);" title="Previous"><i class="ac-icon" data-icon="s-arrow-left"></i></a></li>
                    <li><a href="javascript:void(0);" title="1">1</a></li>
                    <li><a href="javascript:void(0);" title="2">2</a></li>
                    <li class="active"><a href="javascript:void(0);" title="3">3</a></li>
                    <li><a href="javascript:void(0);" title="4">4</a></li>
                    <li><a href="javascript:void(0);" title="5">5</a></li>
                    <li><a href="javascript:void(0);" title="Next"><i class="ac-icon" data-icon="s-arrow-right"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-6 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Dropdown</h3>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="dropdown ac-dropdown text-dark">
                            <a class="dropdown-toggle ac-arrow-no n-pv-10" href="javascript:void(0)" role="button" id="dropdownMenuLinkOne" data-bs-toggle="dropdown" aria-expanded="false" title="Download">Download</a>
                            <div class="dropdown-menu dropdown-menu-left ac-dropdown-1 n-fs-16 text-dark" aria-labelledby="dropdownMenuLinkOne">
                                <a class="dropdown-item" href="#" title="PDF File">PDF File</a>
                                <a class="dropdown-item" href="#" title="CSV File">CSV File</a>
                                <a class="dropdown-item" href="#" title="XLS File">XLS File</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="dropdown ac-dropdown text-dark">
                            <a class="dropdown-toggle ac-arrow-no n-pv-10" href="javascript:void(0)" role="button" id="dropdownMenuLinkTwo" data-bs-toggle="dropdown" aria-expanded="false" title="Download"><i class="ac-icon" data-icon="s-bell"></i></a>
                            <div class="dropdown-menu dropdown-menu-left ac-dropdown-2 n-fs-16 text-dark" aria-labelledby="dropdownMenuLinkTwo">
                                <div class="dd-title text-center">Notification</div>
                                <a href="#" title="Kate Young" class="dropdown-item d-flex align-items-center">
                                    <div class="dd-img">K</div>
                                    <div class="dd-info">
                                        <div class="dd-i-title">Kate Young</div>
                                        <div class="dd-i-sub">Lorem Ipsum is simply dummy text</div>
                                    </div>
                                    <div class="dd-time text-right">3 Mins ago</div>
                                </a>

                                <a href="#" title="Kate Young" class="dropdown-item d-flex align-items-center">
                                    <div class="dd-img"><img src="https://cdn.pixabay.com/photo/2020/09/14/17/17/beach-5571533__340.jpg" alt="" title=""></div>
                                    <div class="dd-info">
                                        <div class="dd-i-title">Kate Young</div>
                                        <div class="dd-i-sub">Lorem Ipsum is simply dummy text of the printing and typesetting.</div>
                                    </div>
                                    <div class="dd-time text-right">3 Mins ago</div>
                                </a>

                                <div class="dd-seell text-center"><a href="#" title="See all incoming activity">See all incoming activity</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Tabs</h3>
                <ul class="nav ac-tabs">
                    <li><a class="active" id="home-tab" data-bs-toggle="tab" href="#home">Home</a></li>
                    <li><a id="profile-tab" data-bs-toggle="tab" href="#profile">Profile</a></li>
                    <li><a id="contact-tab" data-bs-toggle="tab" href="#contact">Contact</a></li>
                </ul>
                <div class="tab-content ac-content">
                    <div class="tab-pane fade show active" id="home">
                        <div class="cms">
                            <p>Placeholder content for the tab panel. This one relates to the home tab. Takes you miles high, so high, 'cause she’s got that one international smile. There's a stranger in my bed, there's a pounding in my head. Oh, no. In another life I would make you stay. ‘Cause I, I’m capable of anything. Suiting up for my crowning battle. Used to steal your parents' liquor and climb to the roof. Tone, tan fit and ready, turn it up cause its gettin' heavy. Her love is like a drug. I guess that I forgot I had a choice.</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile">
                        <div class="cms">
                            <p>Ut eget ullamcorper felis, id ultrices odio. Donec porttitor non felis vitae dapibus. Proin vestibulum erat vel est mattis dictum. Quisque vulputate suscipit massa, ut tincidunt libero commodo sed. Nulla a cursus mi. Nunc suscipit ut lorem vitae tempus. Vestibulum tincidunt nulla ac erat facilisis condimentum. Etiam convallis lacus volutpat pulvinar tempus. Phasellus nec faucibus dolor. Sed elementum tempus lacus, vitae condimentum augue rutrum non. Sed finibus et lacus sit amet maximus. Duis a tincidunt justo, at lobortis orci.</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="contact">
                        <div class="cms">
                            <p>Sed tincidunt ut nibh non bibendum. Donec nisl ligula, lacinia in porttitor sed, rhoncus ac massa. Suspendisse eget sem vel dolor volutpat fringilla et vitae arcu. Nulla pretium blandit enim, et aliquam erat. Donec sit amet sagittis diam, sit amet tincidunt lectus. Aenean quis est sit amet quam vehicula fringilla. Donec vel purus accumsan, congue lacus ut, porta diam. Mauris nec augue sem. Cras sed mauris libero.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Collapse</h3>
                <ul class="ac-collapse accordion" id="accordionExample">
                    <li class="-li">
                        <a class="-tabs " data-bs-toggle="collapse" href="#accordion1" aria-expanded="true" aria-controls="accordion1">What is Lorem Ipsum?<span></span></a>
                        <div id="accordion1" class="-info collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="cms">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            </div>
                        </div>
                    </li>
                    
                    <li class="-li">
                        <a class="-tabs collapsed" data-bs-toggle="collapse" href="#accordion2" aria-expanded="true" aria-controls="accordion2">Why do we use it?<span></span></a>
                        <div id="accordion2" class="-info collapse " aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="cms">
                                <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                            </div>
                        </div>
                    </li>

                    <li class="-li">
                        <a class="-tabs collapsed" data-bs-toggle="collapse" href="#accordion3" aria-expanded="true" aria-controls="accordion3">Where does it come from?<span></span></a>
                        <div id="accordion3" class="-info collapse " aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                            <div class="cms">
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>
                                <p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Form Input</h3>
                <form class="n-mt-20 ac-form">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="firstName">First Name <span class="star">*</span></label>
                                <input type="text" class="form-control ac-input" id="firstName" name="firstName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                                <span class="error">Error Massage Here</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="lastName">Last Name</label>
                                <input type="text" class="form-control ac-input" id="lastName" name="lastName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="email">Email <span class="star">*</span></label>
                                <input type="email" class="form-control ac-input" id="email" name="email" placeholder="" minlength="5" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="phone">Phone</label>
                                <input type="text" class="form-control ac-input" id="phone" name="phone" placeholder="" minlength="7" maxlength="14" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="yourMessage">Your Message</label>
                                <textarea class="form-control ac-textarea" id="yourMessage" name="yourMessage" rows="2" placeholder="" onpaste="return true;" ondrop="return false;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Simple Select</label>
                                <select class="form-control ac-input">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Standard Select Boxes</label>
                                <select class="selectpicker ac-input" data-width="100%" data-size="5" title="Placeholder" data-live-search="true" multiple="">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Checkboxes</label>
                                <div class="ac-checkbox-list">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Checkboxes</label>
                                <div class="ac-checkbox-inline">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Radios</label>
                                <div class="ac-radio-list">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Radio</label>
                                <div class="ac-radio-inline">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Switch</label>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" checked="checked" name=""><span></span>
                                    </label>
                                </span>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" name=""><span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-checkbox-list ac-remember">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Remember my <a href="#" target="_blank" title="Preference">Preference</a><span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-note">
                                    <b>Note:</b> By Clicking Submit, you agree to our <a href="#" target="_blank" title="Privecy Policy">Terms &amp; Conditions</a> and <a href="#" target="_blank" title="Privecy Policy">Privecy Policy</a>.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <img src="assets/images/google-captcha.gif" alt="google-captcha">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group text-sm-right">
                                <button type="submit" class="ac-btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Form Material Input</h3>
                <form class="n-mt-20 ac-form ac-form-md">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="firstName">First Name <span class="star">*</span></label>
                                <input type="text" class="form-control ac-input" id="firstName" name="firstName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                                <span class="error">Error Massage Here</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="lastName">Last Name</label>
                                <input type="text" class="form-control ac-input" id="lastName" name="lastName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="email">Email <span class="star">*</span></label>
                                <input type="email" class="form-control ac-input" id="email" name="email" placeholder="" minlength="5" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="phone">Phone</label>
                                <input type="text" class="form-control ac-input" id="phone" name="phone" placeholder="" minlength="7" maxlength="14" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="yourMessage">Your Message</label>
                                <textarea class="form-control ac-textarea" id="yourMessage" name="yourMessage" rows="2" placeholder="" onpaste="return true;" ondrop="return false;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Simple Select</label>
                                <select class="form-control ac-input">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Standard Select Boxes</label>
                                <select class="selectpicker ac-input" data-width="100%" data-size="5" title="Placeholder" data-live-search="true" multiple="">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Checkboxes</label>
                                <div class="ac-checkbox-list">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Checkboxes</label>
                                <div class="ac-checkbox-inline">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Radios</label>
                                <div class="ac-radio-list">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Radio</label>
                                <div class="ac-radio-inline">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Switch</label>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" checked="checked" name=""><span></span>
                                    </label>
                                </span>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" name=""><span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-checkbox-list ac-remember">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Remember my <a href="#" target="_blank" title="Preference">Preference</a><span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-note">
                                    <b>Note:</b> By Clicking Submit, you agree to our <a href="#" target="_blank" title="Privecy Policy">Terms &amp; Conditions</a> and <a href="#" target="_blank" title="Privecy Policy">Privecy Policy</a>.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <img src="assets/images/google-captcha.gif" alt="google-captcha">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group text-sm-right">
                                <button type="submit" class="ac-btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-4 n-gp-lg-3 n-gm-lg-2">
                <h3 class="ac-iptitle n-mb-15">Form Input Option 1</h3>
                <form class="n-mt-20 ac-form ac-form-wd">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="firstName">First Name <span class="star">*</span></label>
                                <input type="text" class="form-control ac-input" id="firstName" name="firstName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                                <span class="error">Error Massage Here</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="lastName">Last Name</label>
                                <input type="text" class="form-control ac-input" id="lastName" name="lastName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="email">Email <span class="star">*</span></label>
                                <input type="email" class="form-control ac-input" id="email" name="email" placeholder="" minlength="5" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="phone">Phone</label>
                                <input type="text" class="form-control ac-input" id="phone" name="phone" placeholder="" minlength="7" maxlength="14" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="yourMessage">Your Message</label>
                                <textarea class="form-control ac-textarea" id="yourMessage" name="yourMessage" rows="2" placeholder="" onpaste="return true;" ondrop="return false;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Simple Select</label>
                                <select class="form-control ac-input">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-select">
                                <label class="ac-label">Standard Select Boxes</label>
                                <select class="selectpicker ac-input" data-width="100%" data-size="5" title="Placeholder" data-live-search="true" multiple="">
                                    <option>Mustard</option>
                                    <option>Ketchup</option>
                                    <option>Relish</option>
                                    <option>Tent</option>
                                    <option>Flashlight</option>
                                    <option>Toilet Paper</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Checkboxes</label>
                                <div class="ac-checkbox-list">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Checkboxes</label>
                                <div class="ac-checkbox-inline">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Default<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-checkbox">
                                        <input type="checkbox" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Radios</label>
                                <div class="ac-radio-list">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_1" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Inline Radio</label>
                                <div class="ac-radio-inline">
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="1"> Option 1<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="3" disabled=""> Disabled<span></span>
                                    </label>
                                    <label class="ac-radio">
                                        <input type="radio" name="example_2" value="4" checked=""> Checked<span></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group ac-active-label">
                                <label class="ac-label">Default Switch</label>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" checked="checked" name=""><span></span>
                                    </label>
                                </span>
                                <span class="ac-switch">
                                    <label>
                                        <input type="checkbox" name=""><span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-checkbox-list ac-remember">
                                    <label class="ac-checkbox">
                                        <input type="checkbox"> Remember my <a href="#" target="_blank" title="Preference">Preference</a><span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group ac-form-group n-mt-0">
                                <div class="ac-note">
                                    <b>Note:</b> By Clicking Submit, you agree to our <a href="#" target="_blank" title="Privecy Policy">Terms &amp; Conditions</a> and <a href="#" target="_blank" title="Privecy Policy">Privecy Policy</a>.
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <img src="assets/images/google-captcha.gif" alt="google-captcha">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group text-sm-right">
                                <button type="submit" class="ac-btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<?php include('Templates/get_demo.php'); ?>
<?php include('Templates/footer_main.php'); ?>

<div class="modal fade ac-modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-flex flex-column align-items-start">
                <div class="ac-iptitle n-fw-600 n-ff-2 text-white n-lh-130">Modal title</div>
                <div class="n-fs-18 fw-normal text-white n-lh-130">Modal sub title</div>
                <a href="javascript:void(0)" data-bs-dismiss="modal" aria-label="Close" class="ac-close">×</a>
            </div>
            <div class="modal-body">
                <div class="cms">
                    <p>Placeholder content for the tab panel. This one relates to the home tab. Takes you miles high, so high, 'cause she’s got that one international smile. There's a stranger in my bed, there's a pounding in my head. Oh, no. In another life I would make you stay. ‘Cause I, I’m capable of anything. Suiting up for my crowning battle. Used to steal your parents' liquor and climb to the roof. Tone, tan fit and ready, turn it up cause its gettin' heavy. Her love is like a drug. I guess that I forgot I had a choice.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="ac-btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="n-ml-5 ac-btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<?php include('Templates/default/footer.php'); ?>