<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap n-pv-15">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="ac-title text-uppercase">Footer Option 02</div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main_02.php'); ?>

<section class="inner-page-gap n-pv-15">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="ac-title text-uppercase">Footer Option 01</div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>