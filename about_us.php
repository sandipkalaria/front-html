<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap about-us">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 text-center">
                <div class="cms">
                    <h2>Our values mold the way we work with our clients, <br>delivering excellent user experience supported by cutting-edge technologies</h2><br>
                    <p>Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years. Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years.</p>
                </div>
                <div class="-img">
                    <picture>
                        <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/about-us.webp" srcset="assets/images/loader.svg">
                        <img class="lazy" data-src="assets/images/others/about-us.jpg" src="assets/images/loader.svg" alt="" title="" width="900" height="537" />
                    </picture>
                    <a data-fancybox="video" href="https://www.youtube.com/watch?v=_sI_Ps7JSEk" title=""><i class="ac-icon" data-icon="s-play-circle"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inner-page-gap about-us-2 pt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex n-gp-lg-4 n-gm-lg-3 n-gm-md-2">
                <article class="-items n-pa-30 position-relative overflow-hidden n-br-5 d-flex flex-column">
                    <h2 class="ac-iptitle">Careers</h2>
                    <p class="p-tag n-mt-15">Walkout 10 years into your future and feel how it feels to carry on doing the same thing.</p>
                    <div class="mt-auto n-pt-15">
                        <a href="#" class="ac-btn-primary" title="View Positions">View Positions</a>
                    </div>
                </article>
            </div>
            <div class="col-lg-4 col-md-6 d-flex n-gp-lg-4 n-gm-lg-3 n-gm-md-2">
                <article class="-items n-pa-30 position-relative overflow-hidden n-br-5 d-flex flex-column">
                    <h2 class="ac-iptitle">Latest News</h2>
                    <p class="p-tag n-mt-15">This path is just like today, with one difference: you have 10 fewer years remaining in your life. This path is just like today.</p>
                    <div class="mt-auto n-pt-15">
                        <a href="#" class="ac-btn-primary" title="Read Articles">Read Articles</a>
                    </div>
                </article>
            </div>
            <div class="col-lg-4 col-md-6 d-flex n-gp-lg-4 n-gm-lg-3 n-gm-md-2">
                <article class="-items n-pa-30 position-relative overflow-hidden n-br-5 d-flex flex-column">
                    <h2 class="ac-iptitle">Contact</h2>
                    <p class="p-tag n-mt-15">I want you to think about how you will feel in 10 years if you continue doing the exact same things.</p>
                    <div class="mt-auto">
                        <a href="#" class="ac-btn-primary n-mt-15" title="Get in Touch">Get in Touch</a>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>