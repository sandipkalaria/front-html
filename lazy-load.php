<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-001.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-001.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-001.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-001.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-001.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-001.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>                
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-002.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-002.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-002.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-002.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-002.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-002.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-003.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-003.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-003.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-003.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-003.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-003.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>                
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-004.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-004.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-004.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-004.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-004.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-004.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-005.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-005.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-005.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-005.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-005.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-005.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-006.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-006.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-006.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-006.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-006.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-006.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-007.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-007.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-007.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-007.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-007.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-007.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-008.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-008.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-008.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-008.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-008.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-008.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-009.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-009.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-009.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-009.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-009.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-009.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-010.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-010.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-010.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-010.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-010.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-010.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-011.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-011.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-011.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-011.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-011.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-011.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <picture>
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-012.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 767px)" class="lazy-webp" data-srcset="assets/images/lazy-img/mobile-012.jpg" srcset="assets/images/loader.svg">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-012.webp" srcset="assets/images/loader.svg" type="image/webp">
                            <source media="(max-width: 1024px)" class="lazy-webp" data-srcset="assets/images/lazy-img/tablet-012.jpg" srcset="assets/images/loader.svg">
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/lazy-img/desktop-012.webp" srcset="assets/images/loader.svg">
                            <img width="350" height="233" class="lazy" data-src="assets/images/lazy-img/desktop-012.jpg" alt="" src="assets/images/loader.svg" />
                        </picture>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>