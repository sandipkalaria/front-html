# ---------------------------------------------- #
# https://www.aleydasolis.com/htaccess-redirects-generator/
# ---------------------------------------------- #

# ---------------------------------------------- #
# HTTP vs HTTPs URLs 301 Redirect {S} #
# ---------------------------------------------- #
	<IfModule mod_rewrite.c>
		RewriteEngine On
		RewriteCond %{SERVER_PORT} !^443$
		RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
	</IfModule>
# ---------------------------------------------- #
# HTTP vs HTTPs URLs 301 Redirect {E} #
# ---------------------------------------------- #


# ---------------------------------------------- #
# Redirect Non-Www Urls To Www With HTACCESS {S} #
# ---------------------------------------------- #
	<IfModule mod_rewrite.c>
		RewriteEngine On
		RewriteCond %{HTTP_HOST} ^domainname.com$
		RewriteRule (.*) https://www.domainname.com/$1 [R=301,L]
	</IfModule>
# ---------------------------------------------- #
# Redirect Non-Www Urls To Www With HTACCESS {E} #
# ---------------------------------------------- #


# --------------------- #
# ErrorDocument 404 {S} #
# --------------------- #
	<IfModule mod_rewrite.c>
		RewriteEngine On
		ErrorDocument 404 https://www.design.netcluesdemo.com/404/
	</IfModule>
# --------------------- #
# ErrorDocument 404 {E} #
# --------------------- #


# --------------------------- #
# Enable GZIP Compression {S} #
# --------------------------- #
	# --------------------------------------------------- #
	# Gzip compression test 							  #
	# https://varvy.com/tools/gzip/						  #
	# --------------------------------------------------- #
	<IfModule mod_deflate.c>
		# --------------------------------------------------- #
		# Compress HTML, CSS, JavaScript, Text, XML and fonts #
		# --------------------------------------------------- #
		AddOutputFilterByType DEFLATE application/javascript
		AddOutputFilterByType DEFLATE application/rss+xml
		AddOutputFilterByType DEFLATE application/vnd.ms-fontobject
		AddOutputFilterByType DEFLATE application/x-font
		AddOutputFilterByType DEFLATE application/x-font-opentype
		AddOutputFilterByType DEFLATE application/x-font-otf
		AddOutputFilterByType DEFLATE application/x-font-truetype
		AddOutputFilterByType DEFLATE application/x-font-ttf
		AddOutputFilterByType DEFLATE application/x-javascript
		AddOutputFilterByType DEFLATE application/xhtml+xml
		AddOutputFilterByType DEFLATE application/xml
		AddOutputFilterByType DEFLATE font/opentype
		AddOutputFilterByType DEFLATE font/otf
		AddOutputFilterByType DEFLATE font/ttf
		AddOutputFilterByType DEFLATE image/svg+xml
		AddOutputFilterByType DEFLATE image/x-icon
		AddOutputFilterByType DEFLATE text/css
		AddOutputFilterByType DEFLATE text/html
		AddOutputFilterByType DEFLATE text/javascript
		AddOutputFilterByType DEFLATE text/plain
		AddOutputFilterByType DEFLATE text/xml

		# --------------------------------------------------------- #
		# Remove browser bugs (only needed for really old browsers) #
		# --------------------------------------------------------- #
		BrowserMatch ^Mozilla/4 gzip-only-text/html
		BrowserMatch ^Mozilla/4\.0[678] no-gzip
		BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
		Header append Vary User-Agent
	</IfModule>

	# ------------------------------------------------------------------ #
	# Enable Apache Gzip Compression (mod_deflate) in cPanel Account {S} #
	# ------------------------------------------------------------------ #
	# Step 1 - Login to cPanel Account 									 #
	# Step 2 - Open Optimize Website Panel 								 #
	# Step 3 - Enable Apache Gzip Compression 							 #
	# ------------------------------------------------------------------ #
	# Enable Apache Gzip Compression (mod_deflate) in cPanel Account {S} #
	# ------------------------------------------------------------------ #

# --------------------------- #
# Enable GZIP Compression {S} #
# --------------------------- #


# ----------------------------------------------------------------------- #
# If you have create demo/beta then please block/stop google crawling {S} #
# ----------------------------------------------------------------------- #
	#AuthName "Restricted Content" 
 
	#AuthUserFile "/home/designnetcluesde/public_html/front-html/.htpasswd"      
	# ------------------------------------------------------------- #
	# As per your domain name, please change the AuthUserFile path. #
	# ------------------------------------------------------------- #

	#AuthType Basic 
	#require valid-user
# ----------------------------------------------------------------------- #
# If you have create demo/beta then please block/stop google crawling {E} #
# ----------------------------------------------------------------------- #