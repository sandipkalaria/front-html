<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap contact-us-page">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="n-pa-20 n-br-5 -main">
                    <h2 class="ac-iptitle n-mb-15">Contact Details</h2>
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d235013.7071758406!2d72.43965454509866!3d23.020497771806586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1601181013449!5m2!1sen!2sin" height="250" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="n-br-5 n-bs-5 w-100"></iframe>

                    <div class="d-flex align-items-center n-pt-15">
                        <div class="-icon d-flex align-items-center justify-content-center n-bgc-a n-br-5 n-fs-18 text-white text-center"><i class="fa fa-map-marker"></i></div>
                        <div class="n-ml-15 n-fs-14 fw-normal n-lh-140 text-dark">Dummy Address, Lorem Ipsum Sit Amet, <br>Dummy Area, Dummy, Dummy City, Dummy State, <br>Dummy Pin, Dummy Country.</div>
                    </div>

                    <div class="d-flex align-items-center n-pt-15">
                        <div class="-icon d-flex align-items-center justify-content-center n-bgc-a n-br-5 n-fs-18 text-white text-center"><i class="fa fa-phone"></i></div>
                        <div class="n-ml-15 n-fs-14 fw-normal n-lh-140 text-dark">
                            <a class="n-ah-a" href="tel:+19874561234" title="+1 (987) 456 1234">+1 (987) 456 1234</a>
                        </div>
                    </div>

                    <div class="d-flex align-items-center n-pt-15">
                        <div class="-icon d-flex align-items-center justify-content-center n-bgc-a n-br-5 n-fs-18 text-white text-center"><i class="fa fa-envelope"></i></div>
                        <div class="n-ml-15 n-fs-14 fw-normal n-lh-140 text-dark">
                            <a class="n-ah-a" href="tel:info@domainanme.com" title="info@domainanme.com">info@domainanme.com</a>
                        </div>
                    </div>

                    <div class="d-flex align-items-center n-pt-15">
                        <div class="-icon d-flex align-items-center justify-content-center n-bgc-a n-br-5 n-fs-18 text-white text-center"><i class="fa fa-fax"></i></div>
                        <div class="n-ml-15 n-fs-14 fw-normal n-lh-140 text-dark">+1 (987) 456 1234</div>
                    </div>

                    <ul class="ac-share">
                        <li><a href="#" title="" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" title="" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" title="" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" title="" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6">
                <h2 class="ac-iptitle n-mb-15">Get In Touch</h2>
                <div class="cms">
                    <p>We are available by e-mail or by phone. You can also use the quick contact form to ask a question about our services and projects we are working on. We would be pleased to answer your questions.</p>
                </div>
                <form class="n-mt-30 ac-form ac-form-wd">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="firstName">First Name <span class="star">*</span></label>
                                <input type="text" class="form-control ac-input" id="firstName" name="firstName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                                <span class="error">Error Massage Here</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="lastName">Last Name</label>
                                <input type="text" class="form-control ac-input" id="lastName" name="lastName" placeholder="" minlength="1" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="email">Email <span class="star">*</span></label>
                                <input type="email" class="form-control ac-input" id="email" name="email" placeholder="" minlength="5" maxlength="255" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="phone">Phone</label>
                                <input type="text" class="form-control ac-input" id="phone" name="phone" placeholder="" minlength="7" maxlength="14" onpaste="return true;" ondrop="return false;">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="yourMessage">Your Message</label>
                                <textarea class="form-control ac-textarea" id="yourMessage" name="yourMessage" rows="2" placeholder="" onpaste="return true;" ondrop="return false;"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <img src="assets/images/google-captcha.gif" alt="google-captcha">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group text-sm-right">
                                <button type="submit" class="ac-btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>