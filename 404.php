<?php include('Templates/default/header.php'); ?>

<section class="inner-page-gap pt-0 pb-0 not-found-page-1">
    <div class="container">
        <div class="row justify-content-center align-items-center" style="min-height: 100vh">
            <div class="col-lg-8 text-center">
                <h1 class="ac-title">404</h1>
                <div class="n-fs-50 n-fw-600 n-ff-2 n-lh-100 n-fc-a">OOPS!<br />PAGE NOT FOUND</div>
                <div class="n-fs-18 fw-normal n-lh-130 n-mt-20">
                    We're sorry, the page you were looking for isn't found here. The link you followed may either be broken or no longer exists. Please try again, or take a look at our site.
                </div>
                <a href="<?php echo SITE_PATH; ?>" title="Back to Home" class="ac-btn-primary n-mt-20">Back to Home</a>
            </div>
        </div>
    </div>
</section>
</div>
<?php include('Templates/default/footer.php'); ?>