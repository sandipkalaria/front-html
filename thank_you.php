<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<div id="thankyou-page"></div>
<section class="inner-page-gap thankyou-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <div class="-icon"><i class="ac-icon" data-icon="s-mail"></i></div>
                <div class="n-fs-50 fw-light n-lh-100 n-mt-15">Thank you</div>
                <div class="n-fs-18 fw-normal fst-italic n-lh-130 n-mt-15">We appreciate you contacting us about [Contact Reason].<br /> One of our colleagues will get back to you shortly.</div>
                <div class="n-fs-18 n-fw-600 n-fc-a n-lh-130 n-mt-15">Have a great day!</div>
                <a href="<?php echo SITE_PATH; ?>" title="Back to Home" class="ac-btn-primary n-mt-15">Back to Home</a>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>