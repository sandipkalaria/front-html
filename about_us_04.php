<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap about-us-04">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 text-center">
                <h2 class="ac-title n-mb-50">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</h2>
            </div>
        </div>
    </div>
    <div class="-bg">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="cms text-lg-left text-center">
                        <h3><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</strong></h3>
                        <p>Our values mold the way we work with our clients, delivering excellent user experience supported by cutting-edge technologies.</p>
                        <p>Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years. Each week he focused his attention on a different virtue. Over time, through repetition, he hoped to one day experience the pleasure of, He says that he carried out this personal examination for years.</p>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-2">
                    <div class="thumbnail-container object-fit n-bs-5" data-thumb="100%">
                        <div class="thumbnail">
                            <picture>
                                <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/about-us.webp" srcset="assets/images/loader.svg">
                                <img class="lazy" data-src="assets/images/others/about-us.jpg" src="assets/images/loader.svg" alt="" title="" width="900" height="537" />
                            </picture>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>