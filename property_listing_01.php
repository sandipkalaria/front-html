<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<?php include('Templates/property_search.php'); ?>

<!-- No property found S -->
    <section class="inner-page-gap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <picture>
                        <source type="image/webp" class="lazy-webp" data-srcset="assets/images/coming-soon.webp" srcset="assets/images/loader.svg">
                        <img class="lazy" data-src="assets/images/coming-soon.png" src="assets/images/loader.svg" alt="No property found" title="No property found" width="100" height="100">
                    </picture>
                    <div class="ac-title n-mt-5">No property found</div>
                    <div class="p-tag n-mt-15">Sorry we were unable to find any result matching your search criteria. <br>To get more results, try adjusting your search by changing your filters.</div>
                    <div class="n-mt-20">
                        <a href="javascript:void(0);" class="ac-btn-primary" title="Remove all filters">Remove all filters</a>
                        <a href="javascript:void(0);" class="ac-btn-secondary n-mt-5 n-mt-sm-0" title="Need assistance? Contact us">Need assistance? Contact us</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!-- No property found E -->

<?php include('Templates/footer_main_02.php'); ?>
<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>