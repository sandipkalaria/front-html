<section class="home-banner">
    <div id="home-banner" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000" data-bs-pause="string" data-bs-wrap="true">
        <div class="carousel-inner">
            <div class="carousel-item active" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <picture>
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/home-banner-01.webp" srcset="assets/images/loader.svg">
                            <img class="lazy" data-src="assets/images/others/home-banner-01.jpg" src="assets/images/loader.svg" alt="" title="" width="1920" height="1078" />
                        </picture>
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Lorem ipsum dolor sit amet, <br>consectetur adipiscing elit. <br>Nam nec tristique ante.</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <video autoplay>
                            <source src="assets/images/others/html-video-banner.mp4" type="video/mp4">
                            <source src="assets/images/others/html-video-banner.ogg" type="video/ogg">
                        </video>
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Custom HTML Video with Autoplay</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <iframe class="youtube-video" src="https://www.youtube.com/embed/Mxesac55Puo?autoplay=0" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">YouTube Video with Autoplay</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <iframe class="vimeo-video" src="https://player.vimeo.com/video/515708855?h=df229778da&autoplay=1" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Vimeo Video with Autoplay</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <img class="lazyimg" data-src="https://img.youtube.com/vi/Mxesac55Puo/maxresdefault.jpg" src="assets/images/loader.svg" alt="" title="" width="1920" height="1078" />
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">YouTube Video with Poster Image + Zoom Video</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                                <a href="https://www.youtube.com/embed/Mxesac55Puo" data-fancybox title="Play Video" class="ac-btn-primary n-mt-15">Play Video</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <img class="lazyimg" data-src="assets/images/others/home-banner-01.jpg" src="assets/images/loader.svg" alt="" title="" width="1920" height="1078" />
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Vimeo Video with Poster Images + Zoom Video</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                                <a href="https://player.vimeo.com/video/515708855?h=df229778da&autoplay=1" data-fancybox title="Play Video" class="ac-btn-primary n-mt-15">Play Video</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-item" data-bs-interval="5000">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <img class="lazyimg" data-src="assets/images/others/home-banner-01.jpg" src="assets/images/loader.svg" alt="" title="" width="1920" height="1078" />
                    </div>
                </div>
                <div class="-caption d-flex align-items-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <h2 class="n-ts-1 n-fs-20 n-fs-40-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Custom HTML Video with Poster Image + Zoom Video</h2>
                                <div class="n-ts-1 n-fs-16 n-fs-20-lg n-lh-150 n-mt-15 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec egestas lectus.</div>
                                <a href="assets/images/others/html-video-banner.mp4" data-fancybox title="Play Video" class="ac-btn-primary n-mt-15">Play Video</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Indicators S -->
            <ol class="carousel-indicators -indicators">
                <li data-bs-target="#home-banner" data-bs-slide-to="0" class="active"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="1"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="2"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="3"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="4"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="5"></li>
                <li data-bs-target="#home-banner" data-bs-slide-to="6"></li>
            </ol>
        <!-- Indicators E -->

        <!-- Left and right controls S -->
            <a class="-control left ac-btn ac-btn-secondary" href="#home-banner" data-bs-slide="prev" title="Previous"><i class="ac-icon" data-icon="s-arrow-left"></i></a>
            <a class="-control right ac-btn ac-btn-secondary" href="#home-banner" data-bs-slide="next" title="Next"><i class="ac-icon" data-icon="s-arrow-right"></i></a>
        <!-- Left and right controls E -->
    </div>
</section>