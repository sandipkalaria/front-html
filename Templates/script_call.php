<script>
    window.onload = function () {
        $('body').addClass('visibility');
    }
</script>


<?php if (DEVELOPMENTMODE == 'true') { ?>
    <script src="assets/libraries/aos-master/js/aos.js<?php echo FV; ?>"></script>
    <script src="assets/libraries/aos-master/custom/custom.js<?php echo FV; ?>"></script>

    <script src="assets/libraries/popper/popper.min.js<?php echo FV; ?>"></script>
    <script src="assets/libraries/bootstrap/js/bootstrap.min.js<?php echo FV; ?>"></script>
    
    <script src="assets/libraries/lazy/jquery.lazy.min.js<?php echo FV; ?>"></script>
    <script src="assets/libraries/lazy/jquery.lazy.plugins.min.js<?php echo FV; ?>"></script>

    <script src="assets/libraries/OwlCarousel2/js/owl.carousel.min.js<?php echo FV; ?>"></script>

    <script src="assets/libraries/fancybox-master/js/jquery.fancybox.min.js<?php echo FV; ?>"></script>

    <script src="assets/libraries/bootstrap-select-master/js/bootstrap-select.min.js<?php echo FV; ?>"></script>
    <script src="assets/libraries/bootstrap-select-master/custom/js/bootstrap-select-function.js<?php echo FV; ?>"></script>
    <script src="assets/libraries/custom/materialize-src/js/materialize-form.js<?php echo FV; ?>"></script>
        
    <script src="assets/libraries/custom/back-top/js/back-top.js<?php echo FV; ?>"></script>
    
    <script src="assets/libraries/custom/menu/js/menu_01.js<?php echo FV; ?>"></script>

    <script src="assets/libraries/custom/svgicon/svgicon.js<?php echo FV; ?>"></script>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    
    <script src="assets/js/common.js<?php echo FV; ?>"></script>

    <script src="assets/js/test/OwlCarousel2.js<?php echo FV; ?>"></script>
<?php } else { ?>
    <script src="assets/js/minijs/app.min.js<?php echo FV; ?>"></script>
<?php } ?>


<?php if (DEVELOPMENTMODE == 'true') { ?>
    <?php if (PAGE_URL == CONTACT) { ?>
        <script src="assets/libraries/jquery-validation/jquery.validate.min.js"></script>
        <script src="assets/libraries/jquery-validation/additional-methods.min.js"></script>
        <script src="assets/js/pages/contact.js"></script>
    <?php } else if (PAGE_URL == THANKYOU) { ?>
        <script src="assets/js/pages/thankyou.js"></script>
    <?php } else if (PAGE_URL == ABOUT_US) { ?>
        <script src="assets/libraries/fancybox-master/js/jquery.fancybox.min.js<?php echo FV; ?>"></script>
    <?php } ?>
<?php } else { ?>
    <?php if (PAGE_URL == CONTACT) { ?>
        <script src="assets/js/minijs/contact-us.min.js<?php echo FV; ?>"></script>
    <?php } else if (PAGE_URL == THANKYOU) { ?>
        <script src="assets/js/minijs/thank-you.min.js<?php echo FV; ?>"></script>
    <?php } else if (PAGE_URL == ABOUT_US) { ?>
        <script src="assets/js/minijs/about-us.min.js<?php echo FV; ?>"></script>
    <?php } ?>
<?php } ?>