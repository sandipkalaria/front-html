<section class="inner-banner">
    <div id="inner-banner" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000" data-bs-pause="string" data-bs-wrap="true">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="thumbnail-container object-fit">
                    <div class="thumbnail">
                        <picture>
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/home-banner-01.webp" srcset="assets/images/loader.svg">
                            <img class="lazy" data-src="assets/images/others/home-banner-01.jpg" src="assets/images/loader.svg" alt="" title="" width="1920" height="1078" />
                        </picture>
                    </div>
                </div>
                <div class="-caption d-flex align-items-end">
                    <div class="container">
                        <div class="row align-items-end">
                            <div class="col-md-10">
                                <h1 class="n-fs-20 n-fs-30-lg n-fw-600 n-lh-120 n-ff-2 text-white text-capitalize">Coming Soon</h1>
                                <div class="n-fs-16 n-fs-20-lg text-white n-mt-5">Lorem ipsum dolor sit am</div>
                                <ul class="ac-breadcrumb n-mt-15 n-fs-14 fw-normal text-white">
                                    <li><a class="n-ah-b" href="#" title="Home">Home</a></li>
                                    <li><a class="n-ah-b" href="#" title="Coming Soon">Coming Soon</a></li>
                                    <li><a class="n-ah-b" href="#" title="Active">Active</a></li>
                                </ul>
                            </div>
                            <div class="col-md-2 text-md-right n-mt-15 n-mt-md-0">
                                <ul class="-share-print ac-ulli bg-white d-inline-flex n-fc-black">
                                    <li><a class="n-ah-a" href="javascript:void(0);" title="Share" data-toggle="modal" data-target="#shareModal"><i class="ac-icon" data-icon="s-share"></i></a></li>
                                    <li><a class="n-ah-a" href="javascript:void(0);" title="Print"><i class="ac-icon" data-icon="s-printer"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>