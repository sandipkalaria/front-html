<footer>
    <div class="position-relative n-zi-1 n-fs-12 text-white n-bgc-black n-pv-15">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <ul class="ac-ulli justify-content-center justify-content-md-start">
                        <li class="n-mr-15"><a class="n-ah-a" title="Privacy Policy" href="<?php echo JSV; ?>">Privacy Policy</a></li>
                        <li><a class="n-ah-a" title="Site Map" href="<?php echo JSV; ?>">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-md-4 text-center n-mv-5 n-mv-md-0">
                    Copyright &copy; <?php echo date("Y"); ?> <?php echo SITE_NAME; ?> - All Rights Reserved.
                </div>
                <div class="col-md-4 text-center text-md-right">
                    Website Designed &amp; Developed By: <a class="n-ah-a n-fw-bold" href="<?php echo JSV; ?>" target="_blank" rel="nofollow" title="Title Here">Company!</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Cookies S -->
    <!-- <div class="ac-cookies n-zi-99 n-p-fixed">
        <div class="d-flex align-items-center n-pa-10 n-bgc-b n-bs-1 n-br-5">
            <div class="ac-cookies_img"><img class="n-dp-block" src="<?php echo SITE_PATH; ?>assets/images/cookie.svg" alt="Cookies" title="Cookies"></div>
            <div class="n-mh-15">
                <div class="n-fs-14 n-lh-130 n-fw-bold text-white">We use Cookies!:</div>
                <a class="n-fs-14 n-lh-130 n-fw-regular text-white n-ah-a" href="<?php echo SITE_PATH.PRIVACYPOLICY; ?>" title="Read More">Read More</a>
            </div>
            <a class="n-fs-18 n-lh-130 n-fw-bold text-white n-ah-a" href="javascript:void(0)" title="Close"><i class="ac-icon" data-icon="s-x"></i></a>
        </div>
    </div> -->
<!-- Cookies E -->

</div>


<!-- Share With Social Media S -->
    <div class="modal fade ac-modal" id="shareModal" tabindex="-1" aria-labelledby="shareModal" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-sm modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header d-flex flex-column">
                    <div class="n-fs-18 n-fw-400 n-ff-1 text-white n-lh-130">Share With Social Media</div>
                    <a href="javascript:void(0)" data-dismiss="modal" aria-label="Close" class="ac-close">×</a>
                </div>
                <div class="modal-body">
                    <!-- AddToAny BEGIN -->
                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style d-flex justify-content-center">
                            <a class="a2a_button_facebook"></a>
                            <a class="a2a_button_twitter"></a>
                            <a class="a2a_button_whatsapp"></a>
                            <a class="a2a_button_linkedin"></a>
                            <a class="a2a_button_pinterest"></a>
                        </div>
                    <!-- AddToAny END -->
                </div>
            </div>
        </div>
    </div>
<!-- Share With Social Media E -->