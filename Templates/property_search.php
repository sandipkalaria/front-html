<!-- Search S -->
    <section class="n-pv-15 n-bgc-c pro-sea-main d-none d-lg-block">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <ul class="-main-list ac-ulli d-flex flex-wrap justify-content-center">
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Property Name">Property Name</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group ac-form-group n-mb-0">
                                                <label class="ac-label" for="propertyName">Property name</label>
                                                <input type="text" class="form-control ac-input" id="propertyName" name="propertyName" placeholder="" minlength="1" maxlength="255">
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="MLS number">MLS number</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group ac-form-group n-mb-0">
                                                <label class="ac-label" for="MLSNumber">MLS number</label>
                                                <input type="text" class="form-control ac-input" id="MLSNumber" name="MLSNumber" placeholder="" minlength="1" maxlength="255">
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Locations (1)">Locations (1)</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 n-pa-20 grid-3" aria-labelledby="dropdownMenuLinkOne">
                                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                                        <li><span>Location One</span></li>
                                        <li><span>Location Two</span></li>
                                        <li><span>Location Three</span></li>
                                        <li><span>Location Four</span></li>
                                        <li class="active"><span>Location Five</span></li>
                                        <li><span>Location Six</span></li>
                                        <li><span>Location Seven</span></li>
                                        <li><span>Location Eight</span></li>
                                        <li><span>Location Nine</span></li>
                                        <li><span>Location Ten</span></li>
                                    </ul>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Property type (1)">Property type (1)</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 grid-2 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                                        <li><span>Condo</span></li>
                                        <li><span>House</span></li>
                                        <li><span>Commercial</span></li>
                                        <li><span>Semi-detached</span></li>
                                        <li><span>Land</span></li>
                                    </ul>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Beds (1)">Beds (1)</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 grid-5 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                                        <li><span>1+</span></li>
                                        <li><span>2+</span></li>
                                        <li><span>3+</span></li>
                                        <li><span>4+</span></li>
                                        <li><span>5+</span></li>
                                        <li><span>6+</span></li>
                                        <li><span>7+</span></li>
                                        <li><span>8+</span></li>
                                        <li><span>9+</span></li>
                                        <li><span>Any</span></li>
                                    </ul>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Baths (1)">Baths (1)</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 grid-5 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                                        <li><span>1+</span></li>
                                        <li><span>2+</span></li>
                                        <li><span>3+</span></li>
                                        <li><span>4+</span></li>
                                        <li><span>5+</span></li>
                                        <li><span>6+</span></li>
                                        <li><span>7+</span></li>
                                        <li><span>8+</span></li>
                                        <li><span>9+</span></li>
                                        <li><span>Any</span></li>
                                    </ul>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Price">$250K - $1.25M</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="View type (1)">View type (1)</button>
                                <div class="dropdown-menu dropdown-menu-left ac-dropdown-3 grid-2 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                                        <li><span>Beach View</span></li>
                                        <li><span>Beach Front</span></li>
                                        <li><span>Water Front</span></li>
                                        <li><span>Garden View</span></li>
                                        <li><span>Canal Front</span></li>
                                    </ul>                                
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Sqft">Sqft</button>
                                <div class="dropdown-menu dropdown-menu-right ac-dropdown-3 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="dropdown ac-dropdown text-dark">
                                <button type="button" class="ac-btn-outline-primary ac-btn-small ac-arrow-no" id="dropdownMenuLinkOne" data-toggle="dropdown" aria-expanded="false" title="Year Built">Year Built</button>
                                <div class="dropdown-menu dropdown-menu-right ac-dropdown-3 n-pa-20" aria-labelledby="dropdownMenuLinkOne">
                                    <div class="-apply n-mt-15 justify-content-between d-flex">
                                        <a href="javascript:void(0);" title="Clear">Clear</a>
                                        <a href="javascript:void(0);" title="Apply">Apply</a>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li><button type="button" class="ac-btn-primary ac-btn-small" title="Search">Search</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="n-pv-15 n-bgc-c pro-sea-mobile d-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="form-group ac-form-group n-mb-0">
                        <label class="ac-label" for="propertyName">Property name</label>
                        <input type="text" class="form-control ac-input" id="propertyName" name="propertyName" placeholder="" minlength="1" maxlength="255">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group ac-form-group n-mb-0">
                        <label class="ac-label" for="MLSNumber">MLS number</label>
                        <input type="text" class="form-control ac-input" id="MLSNumber" name="MLSNumber" placeholder="" minlength="1" maxlength="255">
                    </div>
                </div>
                <div class="col-12">
                    <label class="ac-label" for="propertyName">Property name</label>
                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                        <li><span>Location One</span></li>
                        <li><span>Location Two</span></li>
                        <li><span>Location Three</span></li>
                        <li><span>Location Four</span></li>
                        <li class="active"><span>Location Five</span></li>
                        <li><span>Location Six</span></li>
                        <li><span>Location Seven</span></li>
                        <li><span>Location Eight</span></li>
                        <li><span>Location Nine</span></li>
                        <li><span>Location Ten</span></li>
                    </ul>
                </div>
                <div class="col-12">
                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                        <li><span>Condo</span></li>
                        <li><span>House</span></li>
                        <li><span>Commercial</span></li>
                        <li><span>Semi-detached</span></li>
                        <li><span>Land</span></li>
                    </ul> 
                </div>
                <div class="col-12">
                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                        <li><span>1+</span></li>
                        <li><span>2+</span></li>
                        <li><span>3+</span></li>
                        <li><span>4+</span></li>
                        <li><span>5+</span></li>
                        <li><span>6+</span></li>
                        <li><span>7+</span></li>
                        <li><span>8+</span></li>
                        <li><span>9+</span></li>
                        <li><span>Any</span></li>
                    </ul>
                </div>
                <div class="col-12">
                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                        <li><span>1+</span></li>
                        <li><span>2+</span></li>
                        <li><span>3+</span></li>
                        <li><span>4+</span></li>
                        <li><span>5+</span></li>
                        <li><span>6+</span></li>
                        <li><span>7+</span></li>
                        <li><span>8+</span></li>
                        <li><span>9+</span></li>
                        <li><span>Any</span></li>
                    </ul>
                </div>
                <div class="col-12"></div>
                <div class="col-12">
                    <ul class="-pro-sea-list ac-ulli d-flex flex-wrap">
                        <li><span>Beach View</span></li>
                        <li><span>Beach Front</span></li>
                        <li><span>Water Front</span></li>
                        <li><span>Garden View</span></li>
                        <li><span>Canal Front</span></li>
                    </ul>
                </div>
                <div class="col-12"></div>
            </div>
        </div>
    </section>
<!-- Search E -->