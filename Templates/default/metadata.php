<?php if (PAGE_URL == HOME) { ?>
<title>Home</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == COMING_SOON) { ?>
<title>Coming Soon</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == CMS) { ?>
<title>Cms</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == CONTACT) { ?>
<title>Contact</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == THANKYOU) { ?>
<title>Thank You</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == SITEMAP) { ?>
<title>Site Map</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == PRIVACYPOLICY) { ?>
<title>Privacy Policy</title>
<meta name="description" content="" />
<?php } else if (PAGE_URL == NOTFOUND) { ?>
<title>404</title>
<meta name="description" content="" />
<?php } else { ?>
<title><?php echo SITE_NAME; ?></title>
<meta name="description" content="" />
<?php } ?>