<!-- Browser Upgrade S -->
    <div class="browser-upgrade">
        <div class="bu-container">
            <div class="bu-1">
                <div class="bu-title">For a better view of the website, update your browser.</div> 
                <div class="bu-subtitle">Those browsers has new features built to bring you the best of the web.</div>
            </div>
            <div class="bu-2">
                <a class="bu-launch-now" href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads" target="_blank" rel="nofollow" title="Update Now">Update Now</a>
            </div>
        </div>
    </div>
<!-- Browser Upgrade E -->