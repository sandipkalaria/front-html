<!-- Organization Schema S -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "<?php echo SITE_NAME; ?>",
        "url": "<?php echo DOMAIN; ?>",
        "logo": "<?php echo SITE_PATH; ?>assets/images/logo.png",
        "email": "<?php echo EMAIL; ?>",
        "telephone": "<?php echo PHONE; ?>",
        "image": ["<?php echo SITE_PATH; ?>assets/images/logo.png"],
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "address": {
            "@type": "PostalAddress",
            "name": "<?php echo ADDRESS; ?>"
        },
        "foundingLocation": {
            "@type": "Place",
            "name": "Cayman Islands"
        }
    }
</script>
<!-- Organization Schema E -->
<!-- Brand Schema S -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "Brand",
        "url": "<?php echo DOMAIN; ?>",
        "image": ["<?php echo SITE_PATH; ?>assets/images/logo.png"],
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
    }
</script>
<!-- Brand Schema E -->
<!-- LocalBusiness Schema S -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "LocalBusiness",
        "name": "<?php echo SITE_NAME; ?>",
        "url": "<?php echo DOMAIN; ?>",
        "openingHours": "Monday-Saturday 8:30am to 5:30pm",
        "email": "<?php echo EMAIL; ?>",
        "logo": "<?php echo SITE_PATH; ?>assets/images/logo.png",
        "image": ["<?php echo SITE_PATH; ?>assets/images/logo.png"],
        "priceRange": "CI$",
        "telephone": "<?php echo PHONE; ?>",
        "address": {
            "@type": "PostalAddress",
            "name": "<?php echo ADDRESS; ?>"
        }
    }
</script>
<!-- LocalBusiness Schema E -->
<!-- Breadcrumb Schema S -->
<script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Books",
            "item": "https://example.com/books"
        },{
            "@type": "ListItem",
            "position": 2,
            "name": "Science Fiction",
            "item": "https://example.com/books/sciencefiction"
        },{
            "@type": "ListItem",
            "position": 3,
            "name": "Award Winners"
        }]
    }
</script>
<!-- Breadcrumb Schema E -->