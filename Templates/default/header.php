<?php
    error_reporting(0);
    include ('Templates/default/config.php');
    include ('Templates/default/HTMLMinifier.php');
?>
<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1" />
    <meta name="theme-color" content="#26a69a" />
    
    <!-- Font CSS S -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&family=Source+Sans+Pro:wght@300;400;600;700&display=swap" rel="stylesheet">
        
        <noscript><link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&family=Source+Sans+Pro:wght@300;400;600;700&display=swap"></noscript> 
    <!-- Font CSS S -->

    <!-- Style Sheet S -->
        <link rel="stylesheet" href="<?php echo SITE_PATH; ?>assets/css/main.css<?php echo FV; ?>" >
    <!-- Style Sheet E -->

    <?php include ('Templates/default/metadata.php'); ?>
    <meta name="author" content="" />
    <meta name="csrf-token" content="">
    
    <!-- OG Meta S -->
        <meta property="og:url" content="" />
        <meta property="og:type" content="" />
        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:image" content="" />
    <!-- OG Meta E -->
    
    <!-- Twitter Meta S -->
        <meta name="twitter:card" content="" />
        <meta name="twitter:title" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:description" content="" />
        <meta name="twitter:image" content="" />
    <!-- Twitter Meta E -->

    <!-- Favicon Icon S -->
        <link rel="icon" href="<?php echo SITE_PATH; ?>assets/images/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo SITE_PATH; ?>assets/images/apple-touch-icon-144.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo SITE_PATH; ?>assets/images/apple-touch-icon-114.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo SITE_PATH; ?>assets/images/apple-touch-icon-72.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo SITE_PATH; ?>assets/images/apple-touch-icon-57.png" />
    <!-- Favicon Icon E -->
    
    <!-- Canonical Link S -->
        <?php $path = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>
        <link rel="canonical" href="<?php echo $path; ?>" />
    <!-- Canonical Link E -->

    <!-- Java Script S -->
        <script src="<?php echo SITE_PATH; ?>assets/js/jquery-3.6.0.min.js<?php echo FV; ?>"></script>
    <!-- Java Script E -->

    <!-- Basic CSS S -->
        <style>
            body{ opacity: 0; }
            .visibility { opacity: 1; }
        </style>
    <!-- Basic CSS E -->
</head>

<body>
    <?php include('Templates/default/google-schema.php'); ?>
    <?php include('Templates/default/browser-upgrade.php'); ?>
    <!-- Scroll To Top S -->
        <div id="back-top" title="Scroll To Top" style="display: none;">
            <i class="ac-icon" data-icon="s-arrow-left"></i>
        </div>
    <!-- Scroll To Top E -->
    <div id="wrapper">