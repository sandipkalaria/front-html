<?php
	session_start();
	error_reporting(0);

	$pagelink = $_SERVER['REQUEST_URI'];
    $pagelink_array = explode('/',$pagelink);
    $page_active = end($pagelink_array);
    define('PAGE_URL', $page_active);

    /* File Versioning S */
    	//$fileVer = '?'.date(dmy);
    	$fileVer = '?'.date(dmyhis);
    	define('FV', $fileVer);
    /* File Versioning E */

    /* Image Versioning S */
    	$imgVer = '?'.'0.1';
    	define('IV', $imgVer);
    /* Image Versioning E */
?>
<?php
	define('SITE_PATH', 'http://localhost/front-html/');

	define('DEVELOPMENTMODE', "true"); // false | true

	define('SITE_NAME', "Company Name");
	define('DOMAIN', "www.domainname.com");	
	define('JSV', "javascript:void(0)");	

	define('HOME', '');
	define('COMING_SOON', 'coming_soon.php');
	define('CMS', 'cms.php');
	define('CONTACT', 'contact_us.php');
	define('THANKYOU', 'thank_you.php');
	define('PRIVACYPOLICY', 'privacy_policy.php');
	define('SITEMAP', 'site_map.php');	
	define('NOTFOUND', '404.php');

	define('ABOUT_US', 'about-us.php');	

	define('PHONE', '+XX-XXXXXXXXXX');
	define('FAX', '+XX-XXXXXXXXXX');
	define('EMAIL', 'info@domainname.com');
	define('ADDRESS', 'XXXX Xxxx Xxxxxxx<br/> Xx. Xxxxx Xxxxxxx, XX XXXXXX.');

	define('FACEBOOK', '#');
	define('TWITTER', '#');
	define('YOUTUBE', '#');
	define('LINKEDIN', '#');
	define('INSTAGRAM', '#');
	define('WHATSAPP', '#');
	define('PINTEREST', '#');
	define('TRIPADVISOR', '#');
?>