<header class="hm-desktop header-main-02">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-flex align-items-center">
                <div class="hm-des-01">
                    <a href="<?php echo SITE_PATH; ?>" title="">
                        <picture>
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/logo.webp" srcset="assets/images/loader.svg">
                            <img class="d-block lazy" data-src="assets/images/others/logo.png" src="assets/images/loader.svg" alt="" title="" width="87" height="25" />
                        </picture>
                    </a>
                </div>
                <div class="hm-des-02 navbar-width">
                    <nav class="menu" id="menu">
                        <ul class="brand-nav brand-navbar" id="menu_ul">
                            <li class="<?php echo ((PAGE_URL == HOME) ? "active":""); ?>"><a href="<?php echo SITE_PATH; ?>" title="Home">Home</a></li>
                            <li><a href="#" title="Dorpdown">Dorpdown</a>
                                <ul class="sub-menu">
                                    <li><a href="<?php echo SITE_PATH.CMS; ?>" title="CMS">CMS</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?php echo SITE_PATH; ?>cms_option" title="CMS 02">CMS Option</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo SITE_PATH.COMING_SOON; ?>" title="Coming Soon">Coming Soon</a></li>
                                    <li><a href="<?php echo SITE_PATH.CONTACT; ?>" title="Contact Us">Contact Us</a></li>
                                    <li><a href="<?php echo SITE_PATH.SITEMAP; ?>" title="Site Map">Site Map</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?php echo SITE_PATH; ?>sitemap-02" title="Site Map 02">Site Map 02</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo SITE_PATH.THANKYOU; ?>" title="Thank you">Thank you</a></li>
                                    <li><a href="<?php echo SITE_PATH.NOTFOUND; ?>" title="404">404</a></li>
                                    <li><a href="<?php echo SITE_PATH; ?>lazy-load" title="Lazy Load">Lazy Load</a>
                                        <ul class="sub-menu">
                                            <li><a href="<?php echo SITE_PATH; ?>lazy-load-02" title="Lazy Load 02">Lazy Load 02</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="<?php echo SITE_PATH; ?>owlcarousel2" title="OwlCarousel2">OwlCarousel2</a></li>
                                </ul>
                            </li>
                            <li><a href="#" title="Multiple">Multiple</a>
                                <ul class="sub-menu">
                                    <li><a href="#" title="Resources">Resources</a></li>
                                    <li><a href="#" title="Tutorials">Tutorials</a>
                                        <ul class="sub-menu">
                                            <li><a href="#" title="HTML/CSS">HTML/CSS</a></li>
                                            <li><a href="#" title="jQuery">jQuery</a></li>
                                            <li><a href="#" title="Other">Other</a>
                                                <ul class="sub-menu">
                                                    <li><a href="#" title="Stuff">Stuff</a></li>
                                                    <li><a href="#" title="Things">Things</a></li>
                                                    <li><a href="#" title="Other Stuff">Other Stuff</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#" title="Links">Links</a></li>
                                </ul>
                            </li>
                            <li><a href="#" title="Mega Menu">Mega Menu</a>
                                <ul class="mega-menu">
                                    <li>
                                        <a href="#" class="mm_title" title="Team and Blog">Team and Blog</a>
                                        <ul>
                                            <li><a href="<?php echo SITE_PATH; ?>team_listing" title="Team Listing 01">Team Listing 01</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>team_listing_02" title="Team Listing 02">Team Listing 02</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>blog_listing" title="Blog Listing 01">Blog Listing 01</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>footer_option" title="Footer Option">Footer Option</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" class="mm_title" title="About Us">About Us</a>
                                        <ul>
                                            <li><a href="<?php echo SITE_PATH; ?>about_us" title="About Us 01">About Us 01</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>about_us_02" title="About Us 02">About Us 02</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>about_us_03" title="About Us 03">About Us 03</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>about_us_04" title="About Us 04">About Us 04</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" class="mm_title" title="Bottoms">Bottoms</a>
                                        <ul>
                                            <li><a href="<?php echo SITE_PATH; ?>property_listing_01" title="Property Listing 01">Property Listing 01</a></li>
                                            <li><a href="<?php echo SITE_PATH; ?>contact_us_02" title="Contact Us 02">Contact Us 02</a></li>
                                            <li><a href="#" title="Capris">Capris</a></li>
                                            <li><a href="#" title="Casual">Casual</a></li>
                                            <li><a href="#" title="Rain">Rain</a></li>
                                            <li><a href="#" title="Winter">Winter</a></li>
                                            <li><a href="#" title="Pants">Pants</a></li>
                                            <li><a href="#" title="Convertible">Convertible</a></li>
                                            <li><a href="#" title="Baselayer">Baselayer</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" class="mm_title" title="Footwear">Footwear</a>
                                        <ul>
                                            <li><a href="#" title="Winter Lifestyle Boots">Winter Lifestyle Boots</a></li>
                                            <li><a href="#" title="Casual">Casual</a></li>
                                            <li><a href="#" title="Flip-Flops">Flip-Flops</a></li>
                                            <li><a href="#" title="Water">Water</a></li>
                                            <li><a href="#" title="Hiking">Hiking</a></li>
                                            <li><a href="#" title="Outdry Waterproof">Outdry Waterproof</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#" class="mm_title" title="Accessories">Accessories</a>
                                        <ul>
                                            <li><a href="#" title="Caps">Caps</a></li>
                                            <li><a href="#" title="Hats">Hats</a></li>
                                            <li><a href="#" title="Neck-Gaiters">Neck-Gaiters</a></li>
                                            <li><a href="#" title="Gloves">Gloves</a></li>
                                            <li><a href="#" title="Beanies">Beanies</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#" title="Tab Menu">Tab Menu</a>
                                <ul class="tab-menu">
                                    <li><a href="#" title="Jackets & Vests">Jackets & Vests</a>
                                        <ul>
                                            <li><a href="#" class="tm_title" title="Rain">Rain</a>
                                                <ul>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Fleece">Fleece</a></li>
                                                    <li><a href="#" title="Insulated & Down">Insulated & Down</a></li>
                                                    <li><a href="#" title="Soft- Shell">Soft- Shell</a></li>
                                                    <li><a href="#" title="Vests">Vests</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Fleece">Fleece</a>
                                                <ul>
                                                    <li><a href="#" title="T-Shirts">T-Shirts</a></li>
                                                    <li><a href="#" title="Polos">Polos</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                    <li><a href="#" title="Shirts">Shirts</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Insulated & Down">Insulated & Down</a>
                                                <ul>
                                                    <li><a href="#" title="Shorts">Shorts</a></li>
                                                    <li><a href="#" title="Capris">Capris</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Winter">Winter</a></li>
                                                    <li><a href="#" title="Pants">Pants</a></li>
                                                    <li><a href="#" title="Convertible">Convertible</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-hidden">
                                                <div class="thumbnail-container">
                                                    <div class="thumbnail">
                                                        <picture>
                                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/default-img.webp" srcset="assets/images/loader.svg">
                                                            <img class="lazy" data-src="assets/images/others/default-img.png" src="assets/images/loader.svg" alt="" title="" width="100" height="100" />
                                                        </picture>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#" title="Tops">Tops</a>
                                        <ul>
                                            <li><a href="#" class="tm_title" title="Insulated & Down">Insulated & Down</a>
                                                <ul>
                                                    <li><a href="#" title="Shorts">Shorts</a></li>
                                                    <li><a href="#" title="Capris">Capris</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Winter">Winter</a></li>
                                                    <li><a href="#" title="Pants">Pants</a></li>
                                                    <li><a href="#" title="Convertible">Convertible</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Fleece">Fleece</a>
                                                <ul>
                                                    <li><a href="#" title="T-Shirts">T-Shirts</a></li>
                                                    <li><a href="#" title="Polos">Polos</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                    <li><a href="#" title="Shirts">Shirts</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Rain">Rain</a>
                                                <ul>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Fleece">Fleece</a></li>
                                                    <li><a href="#" title="Insulated & Down">Insulated & Down</a></li>
                                                    <li><a href="#" title="Soft- Shell">Soft- Shell</a></li>
                                                    <li><a href="#" title="Vests">Vests</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-hidden">
                                                <div class="thumbnail-container">
                                                    <div class="thumbnail">
                                                        <picture>
                                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/default-img.webp" srcset="assets/images/loader.svg">
                                                            <img class="lazy" data-src="assets/images/others/default-img.png" src="assets/images/loader.svg" alt="" title="" width="100" height="100" />
                                                        </picture>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#" title="Bottoms">Bottoms</a>
                                        <ul>
                                            <li><a href="#" class="tm_title" title="Rain">Rain</a>
                                                <ul>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Fleece">Fleece</a></li>
                                                    <li><a href="#" title="Insulated & Down">Insulated & Down</a></li>
                                                    <li><a href="#" title="Soft- Shell">Soft- Shell</a></li>
                                                    <li><a href="#" title="Vests">Vests</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Fleece">Fleece</a>
                                                <ul>
                                                    <li><a href="#" title="T-Shirts">T-Shirts</a></li>
                                                    <li><a href="#" title="Polos">Polos</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                    <li><a href="#" title="Shirts">Shirts</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Insulated & Down">Insulated & Down</a>
                                                <ul>
                                                    <li><a href="#" title="Shorts">Shorts</a></li>
                                                    <li><a href="#" title="Capris">Capris</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Winter">Winter</a></li>
                                                    <li><a href="#" title="Pants">Pants</a></li>
                                                    <li><a href="#" title="Convertible">Convertible</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-hidden">
                                                <div class="thumbnail-container">
                                                    <div class="thumbnail">
                                                        <picture>
                                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/default-img.webp" srcset="assets/images/loader.svg">
                                                            <img class="lazy" data-src="assets/images/others/default-img.png" src="assets/images/loader.svg" alt="" title="" width="100" height="100" />
                                                        </picture>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#" title="Footwear">Footwear</a>
                                        <ul>
                                            <li><a href="#" class="tm_title" title="Insulated & Down">Insulated & Down</a>
                                                <ul>
                                                    <li><a href="#" title="Shorts">Shorts</a></li>
                                                    <li><a href="#" title="Capris">Capris</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Winter">Winter</a></li>
                                                    <li><a href="#" title="Pants">Pants</a></li>
                                                    <li><a href="#" title="Convertible">Convertible</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Fleece">Fleece</a>
                                                <ul>
                                                    <li><a href="#" title="T-Shirts">T-Shirts</a></li>
                                                    <li><a href="#" title="Polos">Polos</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                    <li><a href="#" title="Shirts">Shirts</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Rain">Rain</a>
                                                <ul>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Fleece">Fleece</a></li>
                                                    <li><a href="#" title="Insulated & Down">Insulated & Down</a></li>
                                                    <li><a href="#" title="Soft- Shell">Soft- Shell</a></li>
                                                    <li><a href="#" title="Vests">Vests</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-hidden">
                                                <div class="thumbnail-container">
                                                    <div class="thumbnail">
                                                        <picture>
                                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/default-img.webp" srcset="assets/images/loader.svg">
                                                            <img class="lazy" data-src="assets/images/others/default-img.png" src="assets/images/loader.svg" alt="" title="" width="100" height="100" />
                                                        </picture>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#" title="Accessories">Accessories</a>
                                        <ul>
                                            <li><a href="#" class="tm_title" title="Rain">Rain</a>
                                                <ul>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Fleece">Fleece</a></li>
                                                    <li><a href="#" title="Insulated & Down">Insulated & Down</a></li>
                                                    <li><a href="#" title="Soft- Shell">Soft- Shell</a></li>
                                                    <li><a href="#" title="Vests">Vests</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Fleece">Fleece</a>
                                                <ul>
                                                    <li><a href="#" title="T-Shirts">T-Shirts</a></li>
                                                    <li><a href="#" title="Polos">Polos</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                    <li><a href="#" title="Shirts">Shirts</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#" class="tm_title" title="Insulated & Down">Insulated & Down</a>
                                                <ul>
                                                    <li><a href="#" title="Shorts">Shorts</a></li>
                                                    <li><a href="#" title="Capris">Capris</a></li>
                                                    <li><a href="#" title="Casual">Casual</a></li>
                                                    <li><a href="#" title="Rain">Rain</a></li>
                                                    <li><a href="#" title="Winter">Winter</a></li>
                                                    <li><a href="#" title="Pants">Pants</a></li>
                                                    <li><a href="#" title="Convertible">Convertible</a></li>
                                                    <li><a href="#" title="Baselayer">Baselayer</a></li>
                                                </ul>
                                            </li>
                                            <li class="menu-hidden">
                                                <div class="thumbnail-container">
                                                    <div class="thumbnail">
                                                        <picture>
                                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/default-img.webp" srcset="assets/images/loader.svg">
                                                            <img class="lazy" data-src="assets/images/others/default-img.png" src="assets/images/loader.svg" alt="" title="" width="100" height="100" />
                                                        </picture>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#" title="Option 1">Option 1</a>
                                <ul class="option-1">
                                    <li class="active">
                                        <a href="#" title="Fine Dining">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-fine-dining"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Fine Dining</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Bar & Nightclub">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-bar-nightclub"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Bar & Nightclub</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Casual Dining">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-casual-dining"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Casual Dining</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Cafe Bakery">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-cafe-bakery"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Cafe Bakery</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Fast Casual">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-fast-casual"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Fast Casual</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="Enterprise">
                                            <div class="o1-01 menu-hidden">
                                                <i class="ac-icon" data-icon="s-enterprise"></i>
                                            </div>
                                            <div class="o1-02">
                                                <div class="o1-title">Enterprise</div>
                                                <div class="o1-sub-title menu-hidden">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="#" title="Single Menu 01">Single Menu 01</a></li>
                            <li><a href="#" title="Single Menu 02">Single Menu 02</a></li>
                            <li><a href="#" title="Single Menu 03">Single Menu 03</a></li>
                            <li><a href="#" title="Single Menu 04">Single Menu 04</a></li>
                            <li><a href="#" title="Single Menu 05">Single Menu 05</a></li>
                            <li><a href="#" title="Single Menu 06">Single Menu 06</a></li>
                            <li id="more-menu"><a href="#" title="More">More</a></li>
                        </ul>
                        <ul class="overflow sub-menu"></ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

<header class="hm-mobile header-main-02">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 d-flex align-items-center">
                <div class="hm-mob-01">
                    <a href="<?php echo SITE_PATH; ?>" title="">
                        <picture>
                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/logo.webp" srcset="assets/images/loader.svg">
                            <img class="lazy" data-src="assets/images/others/logo.png" src="assets/images/loader.svg" alt="" title="" width="87" height="25" />
                        </picture>
                    </a>
                </div>
                <div class="hm-mob-02">
                    <a href="#" class="icon-menu n-fs-18" title="Contact Us">Contact Us</a>
                </div>
                <div class="hm-mob-03">
                    <a href="#" title="Email Us" class="icon-menu email-us"><i class="ac-icon" data-icon="s-mail"></i></a>
                </div>
                <div class="hm-mob-04">
                    <a href="#" title="Call Us" class="icon-menu call-us"><i class="ac-icon" data-icon="s-phone-call"></i></a>
                </div>
                <div class="hm-mob-05">
                    <a href="javascript:void(0)" class="icon-menu menu-open" id="menu__open" onclick="openNav()" title="Menu Open"><i class="ac-icon" data-icon="s-menu"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="hm-menu" id="hm-menu">
        <div class="hm-border n-pb-15">
            <table class="w-100">
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="menu-close" id="menu__close" onclick="closeNav()" title="Menu Close"><i class="ac-icon" data-icon="s-arrow-right"></i></a>
                    </td>
                    <td class="text-right n-fs-18 n-fw-600 n-fc-a">Explore Menu</td>
                </tr>
            </table>
        </div>

        <div id="mobile-menu-past"></div>
    </div>
    <div class="nav-overlay" onclick="closeNav()"></div>
</header>
