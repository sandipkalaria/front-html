<footer class="footer_main_02">
    <div class="container n-pv-50">
        <div class="row justify-content-center">
            <div class="col-lg-3 col-12 text-center text-sm-left">
                <a href="<?php echo SITE_PATH; ?>" title="">
                    <picture>
                        <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/logo-black.webp" srcset="assets/images/loader.svg">
                        <img class="-logo lazy" data-src="assets/images/others/logo-black.png" src="assets/images/loader.svg" alt="" title="" width="87" height="25" />
                    </picture>
                </a>

                <div class="cms n-mt-15">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                </div>
            </div>

            <div class="col-lg-3 col-sm-4 col-6 text-center text-sm-left n-mt-20 n-mt-lg-0">
                <h3 class="ac-lptitle n-fc-a text-uppercase">Explore</h3>
                <ul class="ac-ulli flex-column n-mt-20 n-fc-a">
                    <li><a class="n-ah-b" href="#" title="Home">Home</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Dropdown">Dropdown</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Multiple Menu">Multiple Menu</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Mega Menu">Mega Menu</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-sm-4 col-6 text-center text-sm-left n-mt-20 n-mt-lg-0">
                <h3 class="ac-lptitle n-fc-a text-uppercase">Know More</h3>
                <ul class="ac-ulli flex-column n-mt-20 n-fc-a">
                    <li><a class="n-ah-b" href="#" title="Home">Home</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Dropdown">Dropdown</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Multiple Menu">Multiple Menu</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Mega Menu">Mega Menu</a></li>
                </ul>
            </div>

            <div class="col-lg-3 col-sm-4 col-6 text-center text-sm-left n-mt-20 n-mt-lg-0">
                <h3 class="ac-lptitle n-fc-a text-uppercase">About</h3>
                <ul class="ac-ulli flex-column n-mt-20 n-fc-a">
                    <li><a class="n-ah-b" href="#" title="Home">Home</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Dropdown">Dropdown</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Multiple Menu">Multiple Menu</a></li>
                    <li class="n-mt-10"><a class="n-ah-b" href="#" title="Mega Menu">Mega Menu</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="position-relative n-zi-1 n-fs-12 n-fc-a n-pv-15 border-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4">
                    <ul class="ac-ulli text-center text-md-left">
                        <li class="n-mr-15"><a class="n-ah-a" title="Privacy Policy" href="<?php echo JSV; ?>">Privacy Policy</a></li>
                        <li><a class="n-ah-a" title="Site Map" href="<?php echo JSV; ?>">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-md-4 text-center n-mv-5 n-mv-md-0">
                    Copyright &copy; <?php echo date("Y"); ?> <?php echo SITE_NAME; ?> - All Rights Reserved.
                </div>
                <div class="col-md-4 text-center text-md-right">
                    Website Designed &amp; Developed By: <a class="n-ah-a n-fw-bold" href="<?php echo JSV; ?>" target="_blank" rel="nofollow" title="Title Here">Company!</a>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>