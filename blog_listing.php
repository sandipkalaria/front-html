<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap blog-listing">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <?php for ($x = 1; $x <= 3; $x++) { ?>
                        <div class="col-lg-6 col-sm-6 d-flex n-gp-lg-3 n-gm-lg-2">
                            <article class="-items n-a-1 n-bs-5 overflow-hidden d-flex flex-column">
                                <div class="thumbnail-container" data-thumb="66.66%">
                                    <div class="thumbnail">
                                        <picture>
                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/blog-listing.webp" srcset="assets/images/loader.svg">
                                            <img class="lazy" data-src="assets/images/others/blog-listing.jpg" src="assets/images/loader.svg" alt="" title="" width="263" height="175" />
                                        </picture>
                                    </div>
                                    <span class="position-absolute n-zi-9 n-bgc-a n-fs-14 fw-normal text-white n-lh-100 n-pv-5 n-ph-15 n-ma-15 n-br-5 n-bs-5">Category</span>
                                </div>
                                <div class="-main position-relative n-ph-20 n-pt-0 n-pb-20 n-zi-9">
                                    <div class="-author position-absolute n-bs-5 n-bgc-a n-fs-20 f-fw-500 text-white text-center overflow-hidden">SK</div>
                                    <h2 class="ac-lptitle"><a href="#" title="Easier access to Search, Chrome and Gmail in iOS 14">Easier access to Search, Chrome and Gmail in iOS 14</a></h2>
                                    <div class="p-tag n-mt-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                    <span class="n-fs-12 fw-normal text-dark n-lh-100 d-inline-block n-mt-10 n-pv-5 n-ph-10 n-bgc-c n-br-5">April 6, 2020</span>
                                </div>
                            </article>
                        </div>
                        <div class="col-lg-6 col-sm-6 d-flex n-gp-lg-3 n-gm-lg-2">
                            <article class="-items n-a-1 n-bs-5 overflow-hidden d-flex flex-column">
                                <div class="thumbnail-container" data-thumb="66.66%">
                                    <div class="thumbnail">
                                        <picture>
                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/blog-listing.webp" srcset="assets/images/loader.svg">
                                            <img class="lazy" data-src="assets/images/others/blog-listing.jpg" src="assets/images/loader.svg" alt="" title="" width="263" height="175" />
                                        </picture>
                                    </div>
                                    <span class="position-absolute n-zi-9 n-bgc-a n-fs-14 fw-normal text-white n-lh-100 n-pv-5 n-ph-15 n-ma-15 n-br-5 n-bs-5">Category</span>
                                </div>
                                <div class="-main position-relative n-ph-20 n-pt-0 n-pb-20 n-zi-9">
                                    <div class="-author position-absolute n-bs-5 n-bgc-a n-fs-20 f-fw-500 text-white text-center overflow-hidden object-fit">
                                        <picture>
                                            <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/team-listing.webp" srcset="assets/images/loader.svg">
                                            <img class="lazy" data-src="assets/images/others/team-listing.jpg" src="assets/images/loader.svg" alt="" title="" width="263" height="175" />
                                        </picture>
                                    </div>
                                    <h2 class="ac-lptitle"><a href="#" title="Easier access to Search, Chrome and Gmail in iOS 14">Easier access to Search, Chrome and Gmail in iOS 14</a></h2>
                                    <div class="p-tag n-mt-10">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</div>
                                    <span class="n-fs-12 fw-normal text-dark n-lh-100 d-inline-block n-mt-10 n-pv-5 n-ph-10 n-bgc-c n-br-5">April 6, 2020</span>
                                </div>
                            </article>
                        </div>
                    <?php } ?>
                </div>

                <div class="text-center n-mt-25 n-mt-lg-50">
                    <ul class="ac-pagination">
                        <li><a href="javascript:void(0);" title="Previous"><i class="fa fa-angle-left"></i></a></li>
                        <li><a href="javascript:void(0);" title="1">1</a></li>
                        <li><a href="javascript:void(0);" title="2">2</a></li>
                        <li class="active"><a href="javascript:void(0);" title="3">3</a></li>
                        <li><a href="javascript:void(0);" title="4">4</a></li>
                        <li><a href="javascript:void(0);" title="5">5</a></li>
                        <li><a href="javascript:void(0);" title="Next"><i class="fa fa-angle-right"></i></a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-4 n-mt-30 n-mt-lg-0">
                <?php include('blog_left_panel_01.php'); ?>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>