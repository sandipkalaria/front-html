<div class="blog-left-panel-01">
    <article class="bg-white n-pa-25 n-bs-5">
        <h3 class="ac-iptitle n-mb-15">Categories</h3>
        <ul class="-categories n-fs-14 n-fw-500 n-lh-130 text-dark">
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Culture</a></li>
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Creativity</a></li>
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Food</a></li>
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Travel</a></li>
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Humor</a></li>
            <li><a class="n-ah-a d-block n-pv-10" href="#" title="">Music</a></li>
        </ul>
    </article>

    <article class="bg-white n-pa-25 n-bs-5 n-mt-30">
        <h3 class="ac-iptitle n-mb-15">Featured Stories</h3>
        <?php for ($x = 1; $x <= 5; $x++) { ?>
            <div class="-featured n-a-1">
                <div class="-img">
                    <div class="thumbnail-container n-br-5" data-thumb="66.66%">
                        <div class="thumbnail">
                            <picture>
                                <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/blog-listing.webp" srcset="assets/images/loader.svg">
                                <img class="lazy" data-src="assets/images/others/blog-listing.jpg" src="assets/images/loader.svg" alt="" title="" width="263" height="175" />
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="-desc">
                    <a class="n-fs-16 fw-normal n-lh-130 text-dark n-ah-a" href="#">Covid-19 threatens the next generation of smartphones </a>
                    <ul class="ac-ulli n-mt-10 n-fs-13 fw-normal n-lh-130 text-dark">
                        <li class="n-mr-10">Admin</li>
                        <li class="n-mr-10">April 6, 2020</li>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </article>

    <article class="bg-white n-pa-25 n-bs-5 n-mt-30">
        <h3 class="ac-iptitle n-mb-15">Instagram</h3>
    </article>

    <article class="bg-white n-pa-25 n-bs-5 n-mt-30">
        <h3 class="ac-iptitle n-mb-15">Popular Tags</h3>
    </article>
</div>