<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <picture>
                    <source type="image/webp" class="lazy-webp" data-srcset="assets/images/coming-soon.webp" srcset="assets/images/loader.svg">
                    <img class="lazy" data-src="assets/images/coming-soon.png" src="assets/images/loader.svg" alt="Coming Soon" title="Coming Soon"  width="100" height="100">
                </picture>
                <h2 class="ac-title text-uppercase n-mt-5">Coming Soon</h2>
                <div class="n-fs-16 fw-normal">We will back soon.</div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>