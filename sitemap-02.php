<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap sitemap-02">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="sitemap-02-list">
                    <li><a title="Home" href="#">Home</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a>
                                <ul>
                                    <li><a title="Title Here" href="#">Sub Menu Itam</a>
                                        <ul>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">Main Menu Item</a>
                        <ul>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                            <li><a title="Title Here" href="#">Sub Menu Itam</a></li>
                        </ul>
                    </li>
                    <li><a title="Title Here" href="#">Main Menu Item</a></li>
                    <li><a title="Title Here" href="#">XML Sitemap</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/get_demo.php'); ?>
<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>