'use strict';

/* ----------------------------
	Install Node Js
	https://nodejs.org/en/download/
	-----------------------------
	Install Gulp Globally 
	@Note: Only one time for one machine.
	npm install gulp-cli -g
	----------------------------
	STEP:1
	----------------------------
	Open node.js command prompt
	d: [MOVE TO COMPUTER DRIVE]
	cd [PROJECT_FOLDER_PATH]
	After then run below command
	----------------------------
	STEP: 2
	----------------------------
	npm install gulp --save-dev
	npm init
	npm install
---------------------------- */


/* Variable Declared S */
	var gulp				=	require('gulp'),
	    plumber				=	require('gulp-plumber'),
	    notify 				= 	require('gulp-notify'),
	    sass				=	require('gulp-sass'),
	    stripCssComments	=	require('gulp-strip-css-comments'),
	    cleanCSS			=	require('gulp-clean-css'),
	    combineMq			=	require('gulp-combine-mq'),
	    autoprefixer 		= 	require('gulp-autoprefixer'),
	    browserSync			=	require('browser-sync').create(),
	    minify 				= 	require('gulp-minifier'),
	    sourcemaps 			= 	require('gulp-sourcemaps'),
	    concat 				= 	require('gulp-concat'),
	    
	    purify 				= 	require('gulp-purify-css'),

	    sitePath			=	'C:/wamp/www/front-html/',
	    folder = {
	    	designHTML				: 	sitePath,
	        designSource 			: 	sitePath + 'assets/',
	        
	        developmentMainUrl		: 	'localhost/front-html/',
	    };
/* Variable Declared E */


/* Gulpfile Development S */
	/* BrowserSync Processing S */
		function developmentMainUrl(done) {
		    browserSync.init({
		        proxy: folder.developmentMainUrl,
		        //port: 5000
		    });
		    done();
		}
		function browserSyncReload(done) {
		  browserSync.reload();
		  done();
		}
	/* BrowserSync Processing E */

	/* SCSS Processing S */
		function scss() {
		    return gulp.src(folder.designSource + '*.+(scss)')
    	        .pipe(sourcemaps.init())
		        .pipe(sass({
		            outputStyle: 'nested',				// compressed | nested | expanded
		            imagePath: '../images/',
		            precision: 4,
		            errLogToConsole: true,
		        }))
		        .pipe(stripCssComments())
		        .pipe(cleanCSS({compatibility: 'ie8'}))
		        .pipe(combineMq({
					beautify: false					// false | true
			    }))
			    .pipe(autoprefixer({
    	            browsers: ['last 7 versions'],
    	            cascade: true
    	        }))
    	        .pipe(sourcemaps.write('.'))
		        .pipe(gulp.dest(folder.designSource + 'css/'))
		        .pipe(browserSync.stream());
		}
	/* SCSS Processing E */

	/* File Move S */
		function copy() {
	    	return gulp.src(folder.designSource + 'libraries/OwlCarousel2/img/**/*')
	    		.pipe(gulp.dest(folder.designSource + 'images/'));
		}
	/* File Move E */

	/* JS Concat and Minify S */
		function scriptsconcat() {
		    var scriptsoptions = {
	        	minify: true,
	    	    minifyJS: { sourceMap: true },
	    	    getKeptComment: function (content, filePath) {
	    	        var m = content.match(/\/\*![\s\S]*?\*\//img);
	    	        return m && m.join('\n') + '\n' || '';
	    	    }
		    };

		  	gulp.src([
		  		folder.designHTML + 'assets/libraries/custom/materialize-src/js/materialize-form.js',
				folder.designHTML + 'assets/libraries/bootstrap-select-master/js/bootstrap-select.min.js',
				folder.designHTML + 'assets/libraries/bootstrap-select-master/custom/js/bootstrap-select-function.js',
				folder.designHTML + 'assets/libraries/jquery-validation/jquery.validate.min.js',
				folder.designHTML + 'assets/libraries/jquery-validation/additional-methods.min.js'])

		  	  	.pipe(concat('contact-us.min.js'))
		  	  	.pipe(minify(scriptsoptions))
		  	  	.pipe(gulp.dest(folder.designSource + 'js/minijs/'));

		  	gulp.src([
		  		folder.designHTML + 'assets/js/pages/thankyou.js'])
		  	  	.pipe(concat('thank-you.min.js'))
		  	  	.pipe(minify(scriptsoptions))
		  	  	.pipe(gulp.dest(folder.designSource + 'js/minijs/'));
		  	  	
		  	gulp.src([
		  		folder.designHTML + 'assets/libraries/fancybox-master/js/jquery.fancybox.min.js'])
		  	  	.pipe(concat('about-us.min.js'))
		  	  	.pipe(minify(scriptsoptions))
		  	  	.pipe(gulp.dest(folder.designSource + 'js/minijs/'));

		  	return gulp.src([
		  		folder.designHTML + 'assets/libraries/aos-master/js/aos.js', 
		  		folder.designHTML + 'assets/libraries/gsap/minified/gsap.min.js', 
		  		folder.designHTML + 'assets/libraries/aos-master/custom/custom.js', 
		  		folder.designHTML + 'assets/libraries/popper/popper.min.js', 
		  		folder.designHTML + 'assets/libraries/bootstrap/js/bootstrap.min.js', 
		  		folder.designHTML + 'assets/libraries/lazy/jquery.lazy.min.js', 
		  		folder.designHTML + 'assets/libraries/slick/js/slick.min.js', 
		  		folder.designHTML + 'assets/libraries/custom/back-top/js/back-top.js', 
		  		folder.designHTML + 'assets/libraries/custom/menu/js/menu_01.js', 
		  		folder.designHTML + 'assets/libraries/custom/svgicon/svgicon.js', 
		  		folder.designHTML + 'assets/js/common.js'])
		    	.pipe(concat('app.min.js'))
		    	.pipe(minify(scriptsoptions))
		    	.pipe(gulp.dest(folder.designSource + 'js/minijs/'))
		    	.pipe(browserSync.stream());
		}
	/* JS Concat and Minify E */

	/* Watch for Changes S */
		function watch() {
			gulp.watch(folder.designSource + '**/*.scss', gulp.series(scss));
			gulp.watch(folder.designSource + '**/*.js', gulp.series(browserSyncReload));
			gulp.watch(folder.designHTML + '**/*.php', gulp.series(browserSyncReload, scriptsconcat));
		}
	/* Watch for Changes E */
/* Gulpfile Development E */


/* Development and Production Mode S */
	var developmentTaskList = gulp.series(scss, copy);
	
	var development = gulp.parallel(watch, developmentTaskList, developmentMainUrl);

	module.exports = {
	  	scss: scss,
	  	purify: purify,
		copy: copy,
		scriptsconcat: scriptsconcat,
		
		watch: watch,

		developmentTaskList: developmentTaskList,
		development: development,

		default: development,
	};
/* Development and Production Mode E */