<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap about-us-02">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9 text-center">
                <h2 class="ac-iptitle">We enable constant enterprise transformation at speed and scale.</h2>
                <div class="cms n-mt-15">
                    <p>We are a creative company, who works with passion and love. We provide the best services you need. We help you to get better, We take pride in helping our clients deliver marvelous results when it comes to their projects. From data to performance, we’ve got you covered.</p>
                </div>
            </div>
        </div>

        <div class="row n-mt-20 n-mt-md-40">
            <div class="col-md-6 d-flex n-gp-md-3 n-gm-md-2">
                <article class="-items n-bgc-a n-pa-30 d-flex flex-column n-br-5">
                    <h4 class="ac-iptitle text-white">Our Vision</h4>
                    <div class="cms n-mt-15">
                        <p>We are a creative company, who works with passion and love. We provide the best services you need. We help you to get better, We take pride in helping our clients deliver marvelous results when it comes to their projects. From data to performance, we’ve got you covered.</p>
                    </div>
                </article>
            </div>
            <div class="col-md-6 d-flex n-gp-md-3 n-gm-md-2">
                <article class="-items n-bgc-b n-pa-30 d-flex flex-column n-br-5">
                    <h4 class="ac-iptitle text-white">Our Mission</h4>
                    <div class="cms n-mt-15">
                        <p>We also know those epic stories, those modern-day legends surrounding the early failures of such supremely successful folks as Michael Jordan and Bill Gates.</p>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>