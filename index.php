<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/home_banner.php'); ?>

<section class="n-pv-lg-80 n-pv-40">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 order-1 order-lg-0 n-mt-20 n-mt-lg-0">
				<h1 class="ac-title">Lorem Ipsum Dolor Sit Amet, <br>Consectetur Adipiscing Elit.</h1>
				<div class="cms n-mt-20">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec pulvinar augue. Curabitur vitae risus tellus. In ullamcorper, erat vitae malesuada tempus, lectus nunc lacinia sem, fermentum eleifend turpis augue quis lacus. Donec quam est, convallis quis elementum quis, sollicitudin eu tellus. Duis et imperdiet purus. Nullam venenatis laoreet purus, a lobortis lorem congue nec. Vestibulum egestas nibh ligula, et interdum dui lacinia quis. Proin ut purus porttitor neque porttitor dignissim. Nulla eleifend sapien et consequat sagittis. Nulla in sagittis nisi. Maecenas velit justo, rhoncus eu tempor eu, vestibulum non nunc.</p>
				</div>
				<a href="#" title="Read More" class="ac-btn-primary n-mt-20">Read More</a>
			</div>
			<div class="col-lg-5">
				<div class="thumbnail-container object-fit n-bs-5" data-thumb="66.66%" data-thumb-radius="5px">
					<div class="thumbnail">
						<picture>
	                        <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/about-us.webp" srcset="assets/images/loader.svg">
	                        <img class="lazy" data-src="assets/images/others/about-us.jpg" src="assets/images/loader.svg" alt="" title="" width="900" height="537" />
	                    </picture>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>