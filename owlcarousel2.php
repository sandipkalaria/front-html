<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="ac-title text-center">Simple Slider</h2>
                <div class="custom-class owl-slider-1">
                    <div class="owl-carousel owl-theme">
                        <div class="item">1</div>
                        <div class="item">2</div>
                        <div class="item">3</div>
                        <div class="item">4</div>
                        <div class="item">5</div>
                        <div class="item">6</div>
                        <div class="item">7</div>
                        <div class="item">8</div>
                        <div class="item">9</div>
                        <div class="item">10</div>
                        <div class="item">11</div>
                        <div class="item">12</div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-3">
                <h2 class="ac-title text-center">Individual Next Prev Button</h2>
                <div class="custom-class owl-slider-2">
                    <div class="owl-carousel owl-theme">
                        <div class="item">1</div>
                        <div class="item">2</div>
                        <div class="item">3</div>
                        <div class="item">4</div>
                        <div class="item">5</div>
                        <div class="item">6</div>
                        <div class="item">7</div>
                        <div class="item">8</div>
                        <div class="item">9</div>
                        <div class="item">10</div>
                        <div class="item">11</div>
                        <div class="item">12</div>
                    </div>
                    <div class="text-center">
                        <button class="btn-prev" title="Prev">Prev</button>
                        <button class="btn-next" title="Next">Next</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-3">
                <h2 class="ac-title text-center">Left Right Opacity Down</h2>
                <div class="custom-class owl-slider-3">
                    <div class="owl-carousel owl-theme">
                        <div class="item">1</div>
                        <div class="item">2</div>
                        <div class="item">3</div>
                        <div class="item">4</div>
                        <div class="item">5</div>
                        <div class="item">6</div>
                        <div class="item">7</div>
                        <div class="item">8</div>
                        <div class="item">9</div>
                        <div class="item">10</div>
                        <div class="item">11</div>
                        <div class="item">12</div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mt-3">
                <h2 class="ac-title text-center">Animate</h2>
                <div class="custom-class owl-slider-4">
                    <div class="owl-carousel owl-theme">
                        <div class="item">1</div>
                        <div class="item">2</div>
                        <div class="item">3</div>
                        <div class="item">4</div>
                        <div class="item">5</div>
                        <div class="item">6</div>
                        <div class="item">7</div>
                        <div class="item">8</div>
                        <div class="item">9</div>
                        <div class="item">10</div>
                        <div class="item">11</div>
                        <div class="item">12</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<style type="text/css">
    .custom-class {
        margin: 15px 0;
    }
    .custom-class .item{
        padding: 80px 0;
        background-color: #03A9F4;
        border-radius: 5px;
        text-align: center;
        font-size: 50px;
        color: #ffffff;
    }

    .owl-slider-3 .item{
        opacity: .2;
        -webkit-transform: scale3d(0.9, 0.9, 1);
        -khtml-transform: scale3d(0.9, 0.9, 1);
        -moz-transform: scale3d(0.9, 0.9, 1);
        -ms-transform: scale3d(0.9, 0.9, 1);
        -o-transform: scale3d(0.9, 0.9, 1);
        transform: scale3d(0.9, 0.9, 1);
        -webkit-transition: all 0.3s ease-in-out;
        -khtml-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -ms-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
    }
    .owl-slider-3 .owl-item.active.center .item{
        opacity: 1;
        -webkit-transform: scale3d(1.0, 1.0, 1);
        -khtml-transform: scale3d(1.0, 1.0, 1);
        -moz-transform: scale3d(1.0, 1.0, 1);
        -ms-transform: scale3d(1.0, 1.0, 1);
        -o-transform: scale3d(1.0, 1.0, 1);
        transform: scale3d(1.0, 1.0, 1);
    }

    .animated { animation-duration: 1000s; animation-fill-mode: both; }
    .owl-animated-out { z-index: 1 }
    .owl-animated-in { z-index: 0 }

    .fadeOut { -webkit-animation-name: fadeOut; animation-name: fadeOut; }
    @-webkit-keyframes  fadeOut { 0% { opacity: 1; } 100% { opacity: 0; } }
    @keyframes  fadeOut { 0% { opacity: 1; } 100% { opacity: 0; } }

    .fadeIn { -webkit-animation-name: fadeIn; animation-name: fadeIn; }
    @-webkit-keyframes  fadeIn { 0% { opacity: 0; } 100% { opacity: 1; } }
    @keyframes  fadeIn { 0% { opacity: 0; } 100% { opacity: 1; } }
</style>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>