function fromValidation(id){
	$(id).validate({
		rules: {
			acNameR: {
				required: true,
                noSpace: true,
                maxlength: 60,
                check_special_char: true,
                no_url: true,
			},
			acName: {
				maxlength: 60,
				noSpace: {
					depends: function() {
                        if ($("#acName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                check_special_char: {
					depends: function() {
                        if ($("#acName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                no_url: {
					depends: function() {
                        if ($("#acName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acFirstNameR: {
				required: true,
                noSpace: true,
                maxlength: 60,
                check_special_char: true,
                no_url: true,
			},
			acFirstName: {
				maxlength: 60,
				noSpace: {
					depends: function() {
                        if ($("#acFirstName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                check_special_char: {
					depends: function() {
                        if ($("#acFirstName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                no_url: {
					depends: function() {
                        if ($("#acFirstName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acLastNameR: {
				required: true,
                noSpace: true,
                maxlength: 60,
                check_special_char: true,
                no_url: true,
			},
			acLastName: {
				maxlength: 60,
				noSpace: {
					depends: function() {
                        if ($("#acLastName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                check_special_char: {
					depends: function() {
                        if ($("#acLastName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                no_url: {
					depends: function() {
                        if ($("#acLastName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acCompanyNameR: {
				required: true,
                noSpace: true,
                maxlength: 60,
                check_special_char: true,
                no_url: true,
			},
			acCompanyName: {
				maxlength: 60,
				noSpace: {
					depends: function() {
                        if ($("#acCompanyName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                check_special_char: {
					depends: function() {
                        if ($("#acCompanyName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
                no_url: {
					depends: function() {
                        if ($("#acCompanyName").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acEmailR: {
				required: true,
                maxlength: 100,
                emailFormat: true,
			},
			acEmail: {
				maxlength: 100,
                emailFormat: {
					depends: function() {
                        if ($("#acEmail").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
				noSpace: {
					depends: function() {
                        if ($("#acEmail").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acPhoneR: {
				required: true,
				maxlength: 20,
				checkallzero: true,
				phonenumberFormat: true,
				digits: true,
			},
			acPhone: {
				maxlength: 20,
				digits: true,
				checkallzero: {
					depends: function() {
                        if ($("#acPhone").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
				phonenumberFormat: {
					depends: function() {
                        if ($("#acPhone").val() != '') {
                            return true;
                        } else {
                            return false;
                        }
                    }
				},
			},
			acMessagesR: {
				required: true,
				maxlength: 600,
				no_url: true,
				check_special_char: true,
			},
			acMessages: {
				maxlength: 600,
				no_url: true,
				check_special_char: true,
			},
		},
		messages: {
			acNameR: {
				required: "Enter your name.",
				maxlength: "Enter no more than {0} characters.",
			},
			acName: {
				maxlength: "Enter no more than {0} characters.",
			},
			acFirstNameR: {
				required: "Enter your first name.",
				maxlength: "Enter no more than {0} characters.",
			},
			acFirstName: {
				maxlength: "Enter no more than {0} characters.",
			},
			acLastNameR: {
				required: "Enter your last name.",
				maxlength: "Enter no more than {0} characters.",
			},
			acLastName: {
				maxlength: "Enter no more than {0} characters.",
			},
			acCompanyNameR: {
				required: "Enter your company name.",
				maxlength: "Enter no more than {0} characters.",
			},
			acCompanyName: {
				maxlength: "Enter no more than {0} characters.",
			},
			acEmailR: {
				required: "Enter a valid email address.",
				maxlength: "Enter no more than {0} characters.",
			},
			acEmail: {
				maxlength: "Enter no more than {0} characters.",
			},
			acPhoneR: {
				required: "Enter your phone number.",
				maxlength: "Enter no more than {0} characters.",
				digits: "Enter only number.",
			},
			acPhone: {
				maxlength: "Enter no more than {0} characters.",
				digits: "Enter only number.",
			},
			acMessagesR: {
				required: "Enter your messages.",
				maxlength: "Enter no more than {0} characters.",
			},
			acMessages: {
				maxlength: "Enter no more than {0} characters.",
			},
		},
		onfocusout: function(element) {
            this.element(element);
        },
	});
}

jQuery.validator.addMethod("emailFormat", function(value, element) {
    return this.optional(element) || /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/.test(value);
}, 'Enter valid email format.');

jQuery.validator.addMethod("noSpace", function(value, element) {
    if (value.trim().length <= 0) {
        return false;
    } else {
        return true;
    }
}, "No space allowed.");

jQuery.validator.addMethod("check_special_char", function(value, element) {
    if (value != '') {
        if (value.match(/^[\x20-\x7E\n]+$/)) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}, 'Remove special characters or emojis.');

jQuery.validator.addMethod('no_url', function(value, element) {
    var re = /^[a-zA-Z0-9\-\.\:\\]+\.(com|net|org|biz|coop|info|museum|name|pro|edu|gov|int|mil|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|fi|fj|fk|fm|fo|fr|ga|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gv|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rw|ru|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|ws|wf|ye|yt|yu|za|zm|zw|sex)$/;
    var re1 = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    var trimmed = $.trim(value);
    if (trimmed == '') {
        return true;
    }
    if (trimmed.match(re) == null && re1.test(trimmed) == false) {
        return true;
    }
}, "URL not allow.");

jQuery.validator.addMethod('checkallzero', function (value,element) { 
    var zerosReg = /[1-9]/g;
    if (!zerosReg.test(value)) {
        return false;
    } else {
        return true;
    }
}, 'Enter valid number.');

jQuery.validator.addMethod("phonenumberFormat", function(value, element) {
    var newVal = value.replace(/\D/g, '');
    if (newVal.length < 10) {
        return false;
    } else {
        return true;
    }
}, 'Enter 10 digits numbers.');

function acPhoneNumber(e) {
    var t = 0;
    t = document.all ? 3 : document.getElementById ? 1 : document.layers ? 2 : 0;
    if (document.all) e = window.event;
    var n = "";
    var r = "";
    if (t == 2) {
        if (e.which > 0) n = "(" + String.fromCharCode(e.which) + ")";
        r = e.which
    } else {
        if (t == 3) {
            r = window.event ? event.keyCode : e.which
        } else {
            if (e.charCode > 0) n = "(" + String.fromCharCode(e.charCode) + ")";
            r = e.charCode
        }
    }
    if (r >= 65 && r <= 90 || r >= 97 && r <= 122 || r >= 33 && r <= 39 || r >= 42 && r <= 42 || r >= 44 && r <= 44 || r >= 46 && r <= 47 || r >= 58 && r <= 64 || r >= 91 && r <= 96 || r >= 123 && r <= 126 || r == 32) {
        return false
    }
    return true
}