/* OwlCarousel2 Basic S */
    $('.owl-slider-1 .owl-carousel').owlCarousel({
        loop:false,
        rewind:true,
        margin:10,
        /* Show next/prev buttons & dots S */
            nav:false,
            navText: ["Prev","Next"],
            dots:true,
            dotsEach:false,
        /* Show next/prev buttons & dots E*/
        /* Autoplay S */
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false,
            smartSpeed: 1000,
        /* Autoplay E */
        /* Auto Height S */
            autoHeight:false,
        /* Auto Height E */
        /* Lazy Load S */
            lazyLoad:true,
            lazyLoadEager:1,
        /* Lazy Load E */
        /* Responsive S */
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    //loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                480:{
                    items:2,
                    //loop: ($(".owl-carousel .item").length <= 2 ? false : true)
                },
                768:{
                    items:3,
                    //loop: ($(".owl-carousel .item").length <= 3 ? false : true)
                },
                1024:{
                    items: 4,
                    //loop: ($(".owl-carousel .item").length <= 4 ? false : true)
                }
            },
        /* Responsive E */
        /* Mouse & Touch drag enabled / disabled S */
            mouseDrag:true,
            touchDrag:true,
        /* Mouse & Touch drag enabled / disabled E */
        /* Padding left and right on stage S */
            stagePadding:0,
        /* Padding left and right on stage E */
    });
/* OwlCarousel2 Basic E */

/* OwlCarousel2 Basic S */
    $('.owl-slider-2 .owl-carousel').owlCarousel({
        loop:false,
        rewind:true,
        margin:10,
        /* Show next/prev buttons & dots S */
            nav:false,
            navText: ["Prev","Next"],
            dots:true,
            dotsEach:false,
        /* Show next/prev buttons & dots E*/
        /* Autoplay S */
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false,
            smartSpeed: 1000,
        /* Autoplay E */
        /* Auto Height S */
            autoHeight:false,
        /* Auto Height E */
        /* Lazy Load S */
            lazyLoad:true,
            lazyLoadEager:1,
        /* Lazy Load E */
        /* Responsive S */
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    //loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                480:{
                    items:2,
                    //loop: ($(".owl-carousel .item").length <= 2 ? false : true)
                },
                768:{
                    items:3,
                    //loop: ($(".owl-carousel .item").length <= 3 ? false : true)
                },
                1024:{
                    items: 4,
                    //loop: ($(".owl-carousel .item").length <= 4 ? false : true)
                }
            },
        /* Responsive E */
        /* Mouse & Touch drag enabled / disabled S */
            mouseDrag:true,
            touchDrag:true,
        /* Mouse & Touch drag enabled / disabled E */
        /* Padding left and right on stage S */
            stagePadding:0,
        /* Padding left and right on stage E */
    });
    if ($('.owl-slider-2 .owl-carousel').length) {
        $('.btn-prev').on('click', function() {
           $('.owl-slider-2 .owl-carousel').trigger('next.owl.carousel'); 
        });
        $('.btn-next').on('click', function() {
           $('.owl-slider-2 .owl-carousel').trigger('prev.owl.carousel'); 
        });
    }
/* OwlCarousel2 Basic E */

/* OwlCarousel2 Basic S */
    $('.owl-slider-3 .owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        center:true,
        /* Show next/prev buttons & dots S */
            nav:false,
            navText: ["Prev","Next"],
            dots:true,
            dotsEach:false,
        /* Show next/prev buttons & dots E*/
        /* Autoplay S */
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false,
            smartSpeed: 1000,
        /* Autoplay E */
        /* Auto Height S */
            autoHeight:false,
        /* Auto Height E */
        /* Lazy Load S */
            lazyLoad:true,
            lazyLoadEager:1,
        /* Lazy Load E */
        /* Responsive S */
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                480:{
                    items:2,
                    loop: ($(".owl-carousel .item").length <= 2 ? false : true)
                },
                768:{
                    items:3,
                    loop: ($(".owl-carousel .item").length <= 3 ? false : true)
                },
                1024:{
                    items: 3,
                    loop: ($(".owl-carousel .item").length <= 3 ? false : true)
                }
            },
        /* Responsive E */
        /* Mouse & Touch drag enabled / disabled S */
            mouseDrag:true,
            touchDrag:true,
        /* Mouse & Touch drag enabled / disabled E */
        /* Padding left and right on stage S */
            stagePadding:0,
        /* Padding left and right on stage E */
    });
/* OwlCarousel2 Basic E */

/* OwlCarousel2 Basic S */
    $('.owl-slider-4 .owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        /* Show next/prev buttons & dots S */
            nav:false,
            navText: ["Prev","Next"],
            dots:true,
            dotsEach:false,
        /* Show next/prev buttons & dots E*/
        /* Autoplay S */
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:false,
            smartSpeed: 1000,
        /* Autoplay E */
        /* Auto Height S */
            autoHeight:false,
        /* Auto Height E */
        /* Lazy Load S */
            lazyLoad:true,
            lazyLoadEager:1,
        /* Lazy Load E */
        /* Responsive S */
            responsiveClass:true,
            items:1,
            loop: ($(".owl-carousel .item").length <= 1 ? false : true),
            responsive:{
                0:{
                    items:1,
                    loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                480:{
                    items:1,
                    loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                768:{
                    items:1,
                    loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                },
                1024:{
                    items:1,
                    loop: ($(".owl-carousel .item").length <= 1 ? false : true)
                }
            },
        /* Responsive E */
        /* Animate S */
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
        /* Animate E */
        /* Mouse & Touch drag enabled / disabled S */
            mouseDrag:true,
            touchDrag:true,
        /* Mouse & Touch drag enabled / disabled E */
        /* Padding left and right on stage S */
            stagePadding:0,
        /* Padding left and right on stage E */
    });
/* OwlCarousel2 Basic E */