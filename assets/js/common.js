/* Common Js Function S */
    /*jQuery.browser S*/
        (function() {(jQuery.browser = jQuery.browser || {}).mobile = (/android|webos|iphone|ipad|ipod|blackberry/i.test(navigator.userAgent.toLowerCase())); }
        (navigator.userAgent || navigator.vendor || window.opera));
    /*jQuery.browser E*/

    /* Table div Wrap S */
        $(document).ready(function() {
    	    $('.cms table').wrap('<div class="table-responsive"></div>');
        });
    /* Table div Wrap E */

    /* Thumbnail Container Thumb Generator S */
        $(document).ready(function() {
            $(".thumbnail-container").each(function() {
                var dataThum = $( this ).attr("data-thumb");
                $( this ).css("padding-bottom", dataThum);
            });

            $(".thumbnail-container").each(function() {
                var dataThumRadius = $( this ).attr("data-thumb-radius");
                $( this ).css("border-radius", dataThumRadius);
            });
        });
    /* Thumbnail Container Thumb Generator E */

    /* Lazy Loading S */
        function lazyWebpImg () {
            if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                $(function() {
                    $('.lazy').Lazy({
                        afterLoad: function (element) {
                            element.removeClass('lazy');
                        }
                    });
                });
            } else {
                document.addEventListener("DOMContentLoaded", function() {
                    var lazyImages = [].slice.call(document.querySelectorAll("source.lazy-webp"));        
                    if ("IntersectionObserver" in window) {
                        let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
                            entries.forEach(function(entry) {
                                if (entry.isIntersecting) {
                                    let lazyImage = entry.target;
                                    lazyImage.srcset = lazyImage.dataset.srcset;
                                    lazyImage.classList.remove("lazy-webp");
                                    lazyImageObserver.unobserve(lazyImage);
                                }
                            });
                        });
                        lazyImages.forEach(function(lazyImage) {
                            lazyImageObserver.observe(lazyImage);
                        });
                    } else {
                        $(function() {
                            $('.lazy').Lazy({
                                afterLoad: function (element) {
                                    element.removeClass('lazy');
                                }
                            });
                        });
                    }
                });
            }
        }

        function lazyOnlyImg (){
            $(function() {
                $('.lazyimg').Lazy({
                    afterLoad: function (element) {
                        element.removeClass('lazyimg');
                    }
                });
            });
        }

        lazyWebpImg();

        lazyOnlyImg();
    /* Lazy Loading E */
/* Common Js Function E */