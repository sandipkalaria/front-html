<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1" />
    <meta name="robots" content="noindex, nofollow">
    <title>Your Browser JavaScript is off or disabled</title>
</head>

<body>
    <noscript>
        <div style="font-family:'Segoe UI','Segoe WP','Segoe UI Regular','Helvetica Neue',Helvetica,Tahoma,'Arial Unicode MS',Sans-serif;position:fixed;top:0;bottom:0;right:0;left:0;background:#ffffff;z-index:999999999999;letter-spacing:0px;text-align:center;">
            <div style="font-size:30px;font-weight:300;line-height:100%;color:#ff0000;padding:35px 15px 45px 15px;border-bottom:1px solid #cccccc;">Your Browser JavaScript is off or disabled. <br>So Please enable JavaScript and view the full website.</div>
            <div style="font-size:20px;font-weight:400;line-height:120%;color:#008000;padding:35px 15px 20px 15px;">We will be happy to help you. so please visit the below website link<br> and follow the step and enable to your browser JavaScript<br><a href="https://www.whatismybrowser.com/guides/how-to-enable-javascript/" target="_blank" rel="nofollow" title="How to enable JavaScript" style="font-size:20px;font-weight:400;line-height:120%;color:#000000;text-decoration:overline;padding-top:30px;display:inline-block;">How to enable JavaScript</a> </div>
            <div style="font-size: 14px;font-weight:400;line-height:120%;color: #000000;padding:0 15px;">Refresh the page after enabling browser JavaScript. Please press the F5 keyboard button.</div>
        </div>
        <style>
            #wrapper,
            #contact_form,
            #subscription_form,
            #RequestPopup {
                display: none !important;
                opacity: 0 !important;
                visibility: hidden !important;
            }
        </style>
    </noscript>
    <!-- Java Script S -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Java Script E -->
    <script>
        $(document).ready(function() {
            window.location = "http://localhost/project-name/front-html/";
        });
    </script>
</body>

</html>