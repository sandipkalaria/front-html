<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-001.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>                
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-002.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-003.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>                
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-004.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-005.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-006.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-007.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-008.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-009.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-010.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-011.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>

            <div class="col-sm-4 mt-5">
                <div class="thumbnail-container object-fit" style="padding-bottom: 66.66%; background: red">
                    <div class="thumbnail">
                        <img width="350" height="233" class="lazyimg" data-src="assets/images/lazy-img/desktop-012.jpg" alt="" src="assets/images/loader.svg" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>