<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap contact-us-02">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-5">
                <h2 class="ac-title">Get In Touch</h2>

                <form class="n-mt-30 ac-form">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="firstName">First Name <span class="star">*</span></label>
                                <input type="text" class="form-control ac-input" id="firstName" name="firstName" placeholder="" minlength="1" maxlength="255">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="lastName">Last Name</label>
                                <input type="text" class="form-control ac-input" id="lastName" name="lastName" placeholder="" minlength="1" maxlength="255">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="email">Email <span class="star">*</span></label>
                                <input type="email" class="form-control ac-input" id="email" name="email" placeholder="" minlength="5" maxlength="255">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="phone">Phone</label>
                                <input type="text" class="form-control ac-input" id="phone" name="phone" placeholder="" minlength="7" maxlength="14">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group ac-form-group">
                                <label class="ac-label" for="yourMessage">Your Message</label>
                                <textarea class="form-control ac-textarea" id="yourMessage" name="yourMessage" rows="2" placeholder=""></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group">
                                <img src="assets/images/google-captcha.gif" alt="google-captcha">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group ac-form-group text-sm-right">
                                <button type="submit" class="ac-btn-primary" title="Submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xl-6 col-lg-7 offset-xl-1">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d235013.7071758406!2d72.43965454509866!3d23.020497771806586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C%20Gujarat!5e0!3m2!1sen!2sin!4v1601181013449!5m2!1sen!2sin" height="400" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="object-fit n-bs-5 n-mt-20 n-mt-lg-0 w-100"></iframe>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>