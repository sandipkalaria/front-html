<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap team-listing">
    <div class="container">
        <div class="row">
            <?php for ($x = 1; $x <= 12; $x++) { ?>
                <div class="col-lg-3 col-sm-6 n-gp-lg-5 n-gm-lg-3 n-gm-sm-2">
                    <article class="-items n-a-1">
                        <div class="thumbnail-container n-br-5" data-thumb="66.66%">
                            <div class="thumbnail imghvr-img">
                                <picture>
                                    <source type="image/webp" class="lazy-webp" data-srcset="assets/images/others/team-listing.webp" srcset="assets/images/loader.svg">
                                    <img class="lazy" data-src="assets/images/others/team-listing.jpg" src="assets/images/loader.svg" alt="" title="" width="263" height="175" />
                                </picture>
                            </div>
                            <ul class="ac-share -share">
                                <li><a href="#" title="Facebook" target="_blank">FB</a></li>
                                <li><a href="#" title="Twitter" target="_blank">TW</a></li>
                                <li><a href="#" title="Linkedin" target="_blank">LI</a></li>
                                <li><a href="#" title="WhatsApp" target="_blank">WA</a></li>
                            </ul>
                        </div>
                        <div class="n-pt-15">
                            <h2 class="ac-lptitle"><a href="#" title="Dollie Horton">Dollie Horton</a></h2>
                            <div class="p-tag">Marketing</div>
                        </div>
                    </article>
                </div>
            <?php } ?>
            
            <div class="col-12 text-center n-mt-25 n-mt-lg-50">
                <ul class="ac-pagination">
                    <li><a href="javascript:void(0);" title="Previous"><i class="fa fa-angle-left"></i></a></li>
                    <li><a href="javascript:void(0);" title="1">1</a></li>
                    <li><a href="javascript:void(0);" title="2">2</a></li>
                    <li class="active"><a href="javascript:void(0);" title="3">3</a></li>
                    <li><a href="javascript:void(0);" title="4">4</a></li>
                    <li><a href="javascript:void(0);" title="5">5</a></li>
                    <li><a href="javascript:void(0);" title="Next"><i class="fa fa-angle-right"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>