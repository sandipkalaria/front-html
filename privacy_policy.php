<?php include('Templates/default/header.php'); ?>
<?php include('Templates/header_main.php'); ?>
<?php include('Templates/inner_banner.php'); ?>

<section class="inner-page-gap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="coming-soon">
                    <div class="cs-title">Coming Soon</div>
                    <div class="cs-sub-title">We will back soon.</div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include('Templates/footer_main.php'); ?>
<?php include('Templates/default/footer.php'); ?>